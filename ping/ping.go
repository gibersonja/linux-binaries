package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"time"
)

var icmpType byte = 8
var icmpCode byte = 0
var icmpChecksum [2]byte
var icmpIdentifier [2]byte
var icmpSequenceNumber [2]byte

var err error
var destIP string
var icmpMessage []byte
var sleepTime uint64

func main() {
	icmpIdentifier[0] = 0x13
	icmpIdentifier[1] = 0x37
	icmpSequenceNumber[1] = 1
	regex := regexp.MustCompile(`^(\d+)\.(\d+)\.(\d+)\.(\d+)$`)

	if len(os.Args) != 1 {
		args := os.Args[1:]
		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "-h" || arg == "--help" {
				fmt.Print("-h | --help  Print this help message\n")
				fmt.Print("-T | --time  Milliseconds to wait between requests\n")

				return
			}

			if (arg == "-T" || arg == "--time") && i+1 < len(args) {
				sleepTime, err = strconv.ParseUint(args[i+1], 10, 64)
				er(err)
			}

			if regex.MatchString(arg) {
				destIP = arg
			}
		}
	}

	var dataMessage []byte

	for i := 0; i < 3; i++ {
		dataMessage = append(dataMessage, 0x13, 0x37)
	}

	icmpMessage = nil
	icmpMessage = append(icmpMessage, icmpType, icmpCode, icmpChecksum[0], icmpChecksum[1])
	icmpMessage = append(icmpMessage, icmpIdentifier[0], icmpIdentifier[1])
	icmpMessage = append(icmpMessage, icmpSequenceNumber[0], icmpSequenceNumber[1])
	icmpMessage = append(icmpMessage, dataMessage...)

	for i := 1; i < 65536; i++ {
		icmpMessage = nil
		icmpMessage = append(icmpMessage, icmpType, icmpCode, icmpChecksum[0], icmpChecksum[1])
		icmpMessage = append(icmpMessage, icmpIdentifier[0], icmpIdentifier[1])
		icmpMessage = append(icmpMessage, icmpSequenceNumber[0], icmpSequenceNumber[1])
		icmpMessage = append(icmpMessage, dataMessage...)

		icmpSequenceNumber[0] = byte(i >> 8)
		icmpSequenceNumber[1] = byte(i)

		icmpMessage[6] = icmpSequenceNumber[0]
		icmpMessage[7] = icmpSequenceNumber[1]

		cksum := checksum(icmpMessage)

		icmpMessage[2] = cksum[0]
		icmpMessage[3] = cksum[1]

		if sleepTime != 0 {
			time.Sleep(time.Duration(sleepTime) * time.Millisecond)
		}

		startTime := time.Now()
		response := sendMsg(icmpMessage, destIP)
		stopTime := time.Now()

		headerLength := uint16(response[0]<<4>>4) * 4                   // Header length in 32 bit words
		totalLength := (uint16(response[2]) << 8) + uint16(response[3]) // Total lneght including header
		responseMessage := response[headerLength:totalLength]           // Only return the ICMP part of the buffer

		responseTTL := response[8]
		responseTime := time.Time.Sub(stopTime, startTime)
		responseSeq := (uint16(responseMessage[6]) << 8) + uint16(responseMessage[7])

		fmt.Printf("%d bytes from %d.%d.%d.%d: icmp_seq=%d ttl=%d time=%.1f ms\n", len(responseMessage), response[12], response[13], response[14], response[15], responseSeq, responseTTL, float64(responseTime.Nanoseconds())/1000000)

		if i == 65535 {
			i = 1
		}
	}
}

func sendMsg(msg []byte, ip string) []byte {

	conn, err := net.Dial("ip4:1", ip)
	er(err)
	defer conn.Close()

	_, err = conn.Write(msg)
	er(err)

	returnBuffer := make([]byte, 4500)
	_, err = conn.Read(returnBuffer)
	er(err)

	return returnBuffer
}

func checksum(msg []byte) [2]byte {
	var tmp uint32

	if len(msg)%2 != 0 {
		msg = append(msg, 0)
	}

	for i := 0; i < len(msg); i++ {
		if i%2 != 0 {
			tmp += (uint32(msg[i-1]))<<8 + uint32(msg[i])
			tmp = (tmp & 0xFFFF) + (tmp >> 16)
		}
	}

	var cksum [2]byte
	cksum[0] = byte(tmp >> 8)
	cksum[1] = byte(tmp << 8 >> 8)
	cksum[0] = cksum[0] ^ 0xFF
	cksum[1] = cksum[1] ^ 0xFF

	return cksum
}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		if len(cleanup) > 0 {
			if cleanup[0] != "" {
				_, err := os.Stat(cleanup[0])
				er(err, "")
				err = os.Remove(cleanup[0])
				er(err, "")
			}
		}
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
