package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
)

var data []byte

func main() {

	fmt.Println(os.Args[1])
	fmt.Println(ans)
}

func crc(input []byte, bitWidth int) []byte {
	dividend := make([]byte, len(input))
	dividend = input

	var divisor uint64 = 0b100000100110000010001110110110111

	pad := (bitWidth % 8) + (bitWidth / 8)
	for i := 0; i < pad; i++ {
		dividend = append(dividend, 0)
	}

	var asdf bytes.Buffer

	for i := 0; i < len(dividend)-pad; i++ {
		divisor[i] = divisor[i] ^ dividend
	}

	divisor := make([]byte, len(dividend))

	//if bitWidth == 32
	divisor[0] = 0b10000010
	divisor[1] = 0b01100000
	divisor[2] = 0b10001110
	divisor[3] = 0b11011011
	divisor[4] = 0b1

	for i := 0; i < len(input); i++ {
		for j := 0; j < 8; j++ {
			dividend[i] = dividend[i] ^ divisor[i]

			if divisor[i]<<7>>7 == 1 {
				divisor[i] = divisor[i] >> 1
			}

			if divisor[i] != 0 {
				divisor[i] = divisor[i] >> 1
			}
		}

	}
}

/*
func crc(input []byte, bitWidth int, position ...int) []byte {

	for i := 0; i < bitWidth; i++ {
		input = append(input, 0)
	}

	var divisor int

	if bitWidth == 32 {
		divisor = 0b100000100110000010001110110110111
	}

	for i := 0; i < len(input); i++ {
		cursor := i * 8

		if position <
	}
}

func sysv(input []byte) uint16 {
	var sum uint32

	for i := 0; i < len(input); i++ {
		fmt.Println(sum)
		sum += uint32(input[i])
	}

	remainder := sum%(2^16) + (sum%2^32)/(2^16)
	fmt.Printf("Remainder: %d\n", remainder)
	result := (remainder % (2 ^ 16)) + (remainder / (2 ^ 16))

	return uint16(result)
}

func crc32(input []byte) []byte {
	// RFC 3385

	for i := 0; i < 32; i++ {
		input = append(input, 0)
	}

	// CRC32: x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x^1 + x^0
	divisor := 0b100000100110000010001110110110111
	bitCount := 0

	for i := 0; i < len(input); i++ {
		inputElement := input[i]

	}

}
*/

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
