package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
)

var data []byte
var upper bool
var lower bool

func main() {
	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			data = append(data, scanner.Bytes()...)
		}

		err := scanner.Err()
		er(err)
	}

	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("--help  Print this help message\n")
				fmt.Print("-u      Convert to uppercase only\n")
				fmt.Print("-l      Convert to lowercase only\n")
				fmt.Print("Only accepts input from STDIN\n")

				return
			}

			if arg == "-u" {
				upper = true
			}

			if arg == "-l" {
				lower = true
			}
		}
	}

	if !upper && !lower {
		for i := 0; i < len(data); i++ {
			if data[i] > 64 && data[i] < 91 { //Uppercase to Lowercase
				data[i] = data[i] + 32
			} else if data[i] > 96 && data[i] < 123 { //Lowercase to Uppercase
				data[i] = data[i] - 32
			}

			fmt.Printf("%c", data[i])
		}
	}

	if upper && !lower {
		for i := 0; i < len(data); i++ {
			if data[i] > 96 && data[i] < 123 { //Lowercase to Uppercase
				data[i] = data[i] - 32
			}

			fmt.Printf("%c", data[i])
		}
	}

	if !upper && lower {
		for i := 0; i < len(data); i++ {
			if data[i] > 64 && data[i] < 91 { //Uppercase to Lowercase
				data[i] = data[i] + 32
			}
			fmt.Printf("%c", data[i])
		}
	}

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
