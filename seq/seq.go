package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
)

var format bool
var formatString string
var separator bool
var separatorString string
var width bool
var data []int

func main() {
	regexFormat := regexp.MustCompile(`^--format=(\S+)$`)
	regexSeparator := regexp.MustCompile(`^--separator=(\S+)$`)
	regexNumber := regexp.MustCompile(`^\d+$`)

	if len(os.Args) != 1 {
		args := os.Args[1:]
		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("-f, --format=FORMAT\n")
				fmt.Print("       use printf style floating-point FORMAT\n\n")
				fmt.Print("-s, --separator=STRING\n")
				fmt.Print("       use STRING to separate numbers (default: \\n)\n\n")
				fmt.Print("-w, --equal-width\n")
				fmt.Print("       equalize width by padding with leading zeros\n\n")
				fmt.Print("--help display this help and exit\n")

				return
			} else if arg == "-f" || regexFormat.MatchString(arg) {
				format = true
				if arg == "-f" && i+1 < len(args) {
					formatString = args[i+1]
				}

				if regexFormat.MatchString(arg) && len(regexFormat.FindStringSubmatch(arg)) > 1 {
					formatString = regexFormat.FindStringSubmatch(arg)[1]
				}
			} else if arg == "-s" || regexSeparator.MatchString(arg) {
				separator = true
				if arg == "-s" && i+1 < len(args) {
					separatorString = args[i+1]
				}

				if regexSeparator.MatchString(arg) && len(regexSeparator.FindStringSubmatch(arg)) > 1 {
					separatorString = regexSeparator.FindStringSubmatch(arg)[1]
				}
			} else if arg == "-w" || arg == "--equal-width" {
				width = true
			} else if regexNumber.MatchString(arg) {
				number, err := strconv.Atoi(arg)
				er(err)
				data = append(data, number)
			}
		}
	}

	if len(data) < 2 || len(data) > 3 {
		er(fmt.Errorf("accepts only START and END elements with an optional INCRIMENT, given: %v", data))
	}

	var start int = data[0]
	var incriment int = 1
	var end int

	if len(data) == 3 {
		incriment = data[1]
		end = data[2]
	}

	if len(data) == 2 {
		end = data[1]
	}

	if start < end {
		digits := int(math.Floor(math.Log10(float64(end))))
		for i := start; i <= end; i += incriment {
			if width {
				zeros := digits - int(math.Floor(math.Log10(float64(i))))
				for j := 0; j < zeros; j++ {
					fmt.Print("0")
				}
			}
			if separator {
				fmt.Printf("%d", i)
				if i != end {
					fmt.Printf("%s", separatorString)
				}
			} else {
				fmt.Printf("%d\n", i)
			}
		}
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
