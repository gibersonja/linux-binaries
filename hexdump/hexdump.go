package main

import (
	"fmt"
    "io"
	"log"
	"math"
	"os"
	"path"
	"runtime"
	"strconv"
)

func main() {

	var files []string
	var specialChars [33]string
	var startAt int64
	var err error

	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("--help       Print this help message\n")
				fmt.Print("--offset <#> Byte offset to start at (decimal not hexadecimal)\n")

				return
			} else if arg == "--offset" && i+1 < len(args) {
				startAt, err = strconv.ParseInt(args[i+1], 10, 64)
				er(err)
				i++
			} else {
				_, err := os.Stat(arg)
				er(err)

				files = append(files, arg)
			}
		}
	}

	specialChars[0] = "NUL"
	specialChars[1] = "SOH"
	specialChars[2] = "STX"
	specialChars[3] = "ETX"
	specialChars[4] = "EOT"
	specialChars[5] = "ENQ"
	specialChars[6] = "ACK"
	specialChars[7] = "BEL"
	specialChars[8] = "BS "
	specialChars[9] = "TAB"
	specialChars[10] = "LF "
	specialChars[11] = "VT "
	specialChars[12] = "FF "
	specialChars[13] = "CR "
	specialChars[14] = "SO "
	specialChars[15] = "SI "
	specialChars[16] = "DLE"
	specialChars[17] = "DC1"
	specialChars[18] = "DC2"
	specialChars[19] = "DC3"
	specialChars[20] = "DC4"
	specialChars[21] = "NAK"
	specialChars[22] = "SYN"
	specialChars[23] = "ETB"
	specialChars[24] = "CAN"
	specialChars[25] = "EM "
	specialChars[26] = "SUB"
	specialChars[27] = "ESC"
	specialChars[28] = "FS "
	specialChars[29] = "GS "
	specialChars[30] = "RS "
	specialChars[31] = "US "
	specialChars[32] = "SPC"

	for i := 0; i < len(files); i++ {
		file := files[i]
		fileInfo, err := os.Stat(file)
		er(err)

		fh, err := os.Open(file)
		er(err)
		defer fh.Close()

		for j := startAt; j < fileInfo.Size(); j += 16 { //filesize is int64 therefore supports files up to 8 EB
			// Dynamic width file offset

			readBuf := make([]byte, 16)
			_, err = fh.ReadAt(readBuf, j)

			if err != nil && err != io.EOF {
				er(err)
			}

			output := getOffest(fileInfo.Size(), int64(readBuf[0]))
			var hex string
			var ascii string

			for k := 0; k < 16; k++ {
				if k == 0 {
					ascii += "  "
				}
				if k%2 == 0 {
					hex += fmt.Sprint(" ")
				}
				hex += fmt.Sprintf("%02X", readBuf[k])
				if readBuf[k] < 33 {
					ascii += fmt.Sprintf("%s ", specialChars[readBuf[k]])
				} else {
					ascii += fmt.Sprintf("%c   ", readBuf[k])
				}
			}

			output += hex + ascii + "\n"
			fmt.Print(output)

		}

		fmt.Print("\n")
	}

}

func getOffest(fsSize, position int64) string {
	if fsSize < int64(16)*int64(math.Pow(16, 0)) {
		return fmt.Sprintf("0x%01X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 1)) {
		return fmt.Sprintf("0x%02X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 2)) {
		return fmt.Sprintf("0x%03X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 3)) {
		return fmt.Sprintf("0x%04X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 4)) {
		return fmt.Sprintf("0x%05X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 5)) {
		return fmt.Sprintf("0x%06X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 6)) {
		return fmt.Sprintf("0x%07X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 7)) {
		return fmt.Sprintf("0x%08X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 8)) {
		return fmt.Sprintf("0x%09X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 9)) {
		return fmt.Sprintf("0x%010X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 10)) {
		return fmt.Sprintf("0x%011X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 11)) {
		return fmt.Sprintf("0x%012X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 12)) {
		return fmt.Sprintf("0x%013X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 13)) {
		return fmt.Sprintf("0x%014X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 14)) {
		return fmt.Sprintf("0x%015X: ", position)
	} else if fsSize < int64(16)*int64(math.Pow(16, 15)) {
		return fmt.Sprintf("0x%016X: ", position)
	} else {
		return fmt.Sprintf("0x%016x: ", position)
	}
}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		if len(cleanup) > 0 {
			if cleanup[0] != "" {
				_, err := os.Stat(cleanup[0])
				er(err, "")
				err = os.Remove(cleanup[0])
				er(err, "")
			}
		}
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
