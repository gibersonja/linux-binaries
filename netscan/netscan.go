package main

import (
	"fmt"
	"io"
	"io/fs"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

type scanType struct {
	icmp bool
	tcp  bool
	udp  bool
}

var verbose bool = false
var alive bool = false
var outputFile string
var fh fs.FileInfo
var mode scanType
var err error
var fileBuf []string
var timeout time.Duration = 50 * time.Millisecond
var ports []uint16
var wg sync.WaitGroup

func main() {
	mode.icmp = false
	mode.tcp = false
	mode.udp = false

	var ip []uint32

	if len(os.Args) < 2 {
		er(fmt.Errorf("No arguments given"))
	}

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" || arg == "-h" {
			fmt.Print(" -h | --help            Print this help message\n")
			fmt.Print(" -v | --verbose         Verbose output\n")
			fmt.Printf("(-t | --timeout) <int>  Connection timeout in ms, Default: %d ms\n", timeout)
			fmt.Print(" -a | --alive           Only show alive hosts\n")
			fmt.Print("(-o | --output) <file>  Write output to file\n")
			fmt.Printf("--icmp                  Specify ICMP scan, Default: %t\n", mode.icmp)
			fmt.Printf("--tcp                   Specify TCP scan, Default: %t\n", mode.tcp)
			fmt.Printf("--udp                   Specify UDP scan, Default: %t\n", mode.udp)
			fmt.Print("--ports=A-C,E,G         Specify ports to scan, to be used with --tcp or --udp\n")
			fmt.Print("A.B.C.D                 Single IP address to scan\n")
			fmt.Print("A.B.C.D[/XY]            IP address and mask bit to specify scan range\n")
			fmt.Print("A.B.C.D-E.F.G.H         IP address range to scan\n")

			return
		}

		if arg == "--verbose" || arg == "-v" {
			verbose = true
		}

		if arg == "--icmp" {
			mode.icmp = true
		}

		if arg == "--tcp" {
			mode.tcp = true
		}

		if arg == "--udp" {
			mode.udp = true
		}

		regex := regexp.MustCompile(`--ports=(\S+)$`)
		if regex.MatchString(arg) {
			tmp := regex.FindStringSubmatch(arg)[1]
			commaSeperated := strings.Split(tmp, ",")
			var tmpResult []string
			regex2 := regexp.MustCompile(`^(\d+)-(\d+)$`)
			regex3 := regexp.MustCompile(`^\d+$`)
			for i := 0; i < len(commaSeperated); i++ {
				if regex3.MatchString(commaSeperated[i]) {
					tmpResult = append(tmpResult, commaSeperated[i])
				}
				if regex2.MatchString(commaSeperated[i]) {
					startport, err := strconv.ParseUint(regex2.FindStringSubmatch(commaSeperated[i])[1], 10, 16)
					er(err)
					stopport, err := strconv.ParseUint(regex2.FindStringSubmatch(commaSeperated[i])[2], 10, 16)
					er(err)
					for j := startport; j <= stopport; j++ {
						tmpResult = append(tmpResult, fmt.Sprintf("%d", j))
					}
				}
			}
			for j := 0; j < len(tmpResult); j++ {
				tmpPort, err := strconv.ParseUint(tmpResult[j], 10, 16)
				er(err)
				ports = append(ports, uint16(tmpPort))
			}
		}

		if (arg == "-o" || arg == "--output") && n+1 < len(args) {
			outputFile = args[n+1]
		}

		if (arg == "-t" || arg == "--timeout") && n+1 < len(args) {
			tmp, err := strconv.Atoi(args[n+1])
			er(err)
			timeout = time.Duration(tmp) * time.Millisecond
		}

		if arg == "-a" || arg == "--alive" {
			alive = true
		}

		regex = regexp.MustCompile(`^(\d+)\.(\d+)\.(\d+)\.(\d+)$`)

		if regex.MatchString(arg) {
			var tmpIP uint32

			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 24)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[2])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 16)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[3])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 8)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[4])
			er(err)
			tmpIP = tmpIP + (uint32(tmp))

			ip = append(ip, tmpIP)
		}

		regex = regexp.MustCompile(`^(\d+)\.(\d+)\.(\d+)\.(\d+)-(\d+)\.(\d+)\.(\d+)\.(\d+)$`)

		if regex.MatchString(arg) {
			var tmpIP uint32

			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 24)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[2])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 16)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[3])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 8)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[4])
			er(err)
			tmpIP = tmpIP + (uint32(tmp))

			ip = append(ip, tmpIP)
			tmpIP = 0

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[5])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 24)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[6])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 16)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[7])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 8)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[8])
			er(err)
			tmpIP = tmpIP + (uint32(tmp))

			ip = append(ip, tmpIP)
		}

		regex = regexp.MustCompile(`^(\d+)\.(\d+)\.(\d+)\.(\d+)\/(\d+)$`)

		if regex.MatchString(arg) {
			var tmpIP uint32
			var tmpMask uint32

			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 24)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[2])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 16)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[3])
			er(err)
			tmpIP = tmpIP + (uint32(tmp) << 8)

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[4])
			er(err)
			tmpIP = tmpIP + (uint32(tmp))

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[5])

			for i := 0; i < tmp; i++ {
				tmpMask = tmpMask << 1
				tmpMask = tmpMask + 1
			}
			for i := 0; i < 32-tmp; i++ {
				tmpMask = tmpMask << 1
			}

			firstIP := tmpIP & tmpMask
			lastIP := tmpIP | ^tmpMask

			ip = append(ip, firstIP)
			ip = append(ip, lastIP)
		}
	}

	if len(ports) == 0 {
		for i := uint16(0); i < 1025; i++ {
			ports = append(ports, i)
		}
	}

	var counter uint16

	if mode.icmp {
		//fmt.Print("ICMP RESPONSE:\n")
		if outputFile != "" {
			fileBuf = append(fileBuf, "ICMP RESPONSE:\n")
		}
		if len(ip) == 1 {
			wg.Add(1)
			go ping(ip[0], counter, &wg)
			counter++
		} else {
			for i := ip[0]; i <= ip[1]; i++ {
				wg.Add(1)
				go ping(i, counter, &wg)
				counter++
			}
		}
	}

	if mode.tcp {
		//fmt.Print("TCP RESPONSE:\n")
		if outputFile != "" {
			fileBuf = append(fileBuf, "TCP RESPONSE:\n")
		}
		if len(ip) == 1 {
			for i := 0; i < len(ports); i++ {
				wg.Add(1)
				go tcp(ip[0], ports[i], &wg)
			}
		} else {
			for j := ip[0]; j <= ip[1]; j++ {
				for i := 0; i < len(ports); i++ {
					wg.Add(1)
					go tcp(j, ports[i], &wg)
				}
			}
		}

	}

	if mode.udp {
		fmt.Print("UDP RESPONSE:\n")
		if outputFile != "" {
			fileBuf = append(fileBuf, "TCP RESPONSE:\n")
		}
		if len(ip) == 1 {
			for i := 0; i < len(ports); i++ {
				udp(ip[0], ports[i])
			}
		} else {
			for j := ip[0]; j <= ip[1]; j++ {
				for i := 0; i < len(ports); i++ {
					udp(j, ports[i])
				}
			}
		}
	}

	if outputFile != "" {
		fh, err := os.OpenFile(outputFile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
		er(err)

		defer fh.Close()

		for i := 0; i < len(fileBuf); i++ {
			_, err := fh.WriteString(fileBuf[i])
			er(err)
		}
	}
	wg.Wait()
}

func debug(input string) {
	if verbose {
		fmt.Printf("%s\n", input)
	}
}

func tcp(addr uint32, port uint16, wg *sync.WaitGroup) {
	defer wg.Done()
	ip := fmt.Sprintf("%d.%d.%d.%d", byte(addr>>24), byte(addr>>16), byte(addr>>8), byte(addr))
	debug(fmt.Sprintf("TCP Connection To: %s:%d", ip, port))
	conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", ip, port), timeout)
	var output string
	if err != nil {
		if !alive {
			output = fmt.Sprintf("  %s:%d\tCLOSED\n", ip, port)
		}
	}
	if conn != nil {
		defer conn.Close()
		output = fmt.Sprintf("  %s:%d\tOPEN\n", ip, port)
	}
	fmt.Print(output)
	if outputFile != "" {
		fileBuf = append(fileBuf, output)
	}
}

func udp(addr uint32, port uint16) {
	ip := fmt.Sprintf("%d.%d.%d.%d", byte(addr>>24), byte(addr>>16), byte(addr>>8), byte(addr))
	debug(fmt.Sprintf("UPD Connection To: %s:%d", ip, port))
	conn, err := net.DialTimeout("udp", fmt.Sprintf("%s:%d", ip, port), timeout)
	defer conn.Close()
	if err != nil {
		if !alive {
			fmt.Printf("  %s:%d\tCLOSED\n", ip, port)
			if outputFile != "" {
				fileBuf = append(fileBuf, fmt.Sprintf("  %s:%d\tCLOSED\n", ip, port))
			}
		}
		return
	}
	if conn != nil {
		udpBuf := make([]byte, 2000)
		err = conn.SetReadDeadline(time.Now().Add(timeout))
		er(err)
		err = conn.SetWriteDeadline(time.Now().Add(timeout))
		er(err)
		_, err = conn.Write([]byte("UDP TEST"))
		er(err)
		_, err = conn.Read(udpBuf)
		var bufSize int
		for i := 0; i < len(udpBuf); i++ {
			bufSize = bufSize + int(udpBuf[i])
		}

		var output string
		if bufSize > 0 {
			output = fmt.Sprintf("  %s:%d\tOPEN\n", ip, port)
		} else {
			if !alive {
				output = fmt.Sprintf("  %s:%d\tCLOSED\n", ip, port)
			}
		}

		fmt.Print(output)
		if output != "" {
			fileBuf = append(fileBuf, output)
		}
		defer conn.Close()
	}
}

func ping(addr uint32, counter uint16, wg *sync.WaitGroup) {
	defer wg.Done()
	var packet []byte

	// TYPE
	packet = append(packet, byte(8))
	// CODE
	packet = append(packet, byte(0))
	// CHECKSUM
	packet = append(packet, byte(0))
	packet = append(packet, byte(0))
	// IDENTIFIER
	packet = append(packet, byte(0))
	packet = append(packet, byte(1))
	// SEQUENCE NUMBER
	packet = append(packet, byte(counter>>8))
	packet = append(packet, byte(counter))
	// DATA
	packet = append(packet, byte(addr>>24))
	packet = append(packet, byte(addr>>16))
	packet = append(packet, byte(addr>>8))
	packet = append(packet, byte(addr))

	var currentChecksum uint32

	for i := 0; i < len(packet); i++ {
		if i%2 == 0 {
			currentChecksum += (uint32(packet[i]) << 8) + uint32(packet[i+1])
			currentChecksum = (currentChecksum & 0xFFFF) + (currentChecksum >> 16)
		}
	}

	packet[2] = byte(currentChecksum>>8) ^ 0xFF
	packet[3] = byte(currentChecksum) ^ 0xFF

	debug("Sending Packet:")
	debugPacketICMP(packet)
	conn, err := net.Dial("ip4:1", fmt.Sprintf("%d.%d.%d.%d", byte(addr>>24), byte(addr>>16), byte(addr>>8), byte(addr)))
	er(err)
	defer conn.Close()

	err = conn.SetDeadline(time.Now().Add(timeout))
	er(err)

	_, err = conn.Write(packet)
	er(err)

	replyPacket := make([]byte, 128)

	for {
		_, err = conn.Read(replyPacket)
		if err != nil {
			if err != io.EOF {
				errNet, _ := err.(net.Error)
				if net.Error.Timeout(errNet) {
					if !alive {
						output := fmt.Sprintf("  %d.%d.%d.%d:ICMP\tTIMEOUT\n", byte(addr>>24), byte(addr>>16), byte(addr>>8), byte(addr))
						fmt.Print(output)
						if outputFile != "" {
							fileBuf = append(fileBuf, output)
						}
					}
				} else {
					fmt.Printf("Read Error: %v\n", err)
				}
				break
			} else {
				er(err)
			}
		}

		//Offset of 20 bytes
		replyPacket = replyPacket[20:]

		debug("Received Packet:")
		debugPacketICMP(replyPacket[:12])

		if replyPacket[8] == byte(addr>>24) && replyPacket[9] == byte(addr>>16) && replyPacket[10] == byte(addr>>8) && replyPacket[11] == byte(addr) {
			output := fmt.Sprintf("  %d.%d.%d.%d:ICMP\tALIVE\n", byte(addr>>24), byte(addr>>16), byte(addr>>8), byte(addr))
			fmt.Print(output)
			if outputFile != "" {
				fileBuf = append(fileBuf, output)
			}
		}
		return
	}
}

func debugPacketICMP(packet []byte) {
	debug(fmt.Sprintf("  Type: %d", packet[0]))
	debug(fmt.Sprintf("  Code: %d", packet[1]))
	debug(fmt.Sprintf("  Cksum:%d", (uint16(packet[2])<<8)+uint16(packet[3])))
	debug(fmt.Sprintf("  Ident:%d", (uint16(packet[4])<<8)+uint16(packet[5])))
	debug(fmt.Sprintf("  Seq:  %d", (uint16(packet[6])<<8)+uint16(packet[7])))
	for i := 8; i < len(packet); i++ {
		debug(fmt.Sprintf("  Data: %d", packet[i]))
	}

	debug(fmt.Sprintf("\n"))

	debug(fmt.Sprintf("  Type: %02X", packet[0]))
	debug(fmt.Sprintf("  Code: %02X", packet[1]))
	debug(fmt.Sprintf("  Cksum:%02X", packet[2]))
	debug(fmt.Sprintf("  Cksum:%02X", packet[3]))
	debug(fmt.Sprintf("  Ident:%02X", packet[4]))
	debug(fmt.Sprintf("  Ident:%02X", packet[5]))
	debug(fmt.Sprintf("  Seq:  %02X", packet[6]))
	debug(fmt.Sprintf("  Seq:  %02X", packet[7]))
	for i := 8; i < len(packet); i++ {
		debug(fmt.Sprintf("  Data: %02X", packet[i]))
	}
}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
