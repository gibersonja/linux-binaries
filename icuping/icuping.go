package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
)

func main() {
	if len(os.Args) < 2 {
		er(fmt.Errorf("no arguments given"))
	}

	var ip string
	var typ uint8 = 8
	var code uint8
	var gw [4]byte
	var cksum uint16
	var data uint64
	var ptr uint8
	var id uint16
	var seq uint16
	var org_t uint32
	var rcv_t uint32
	var trn_t uint32
	var msg []byte
	var verbose bool = true
	var regex *regexp.Regexp
	var err error

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" {
			fmt.Print("--type=<#>     ICMP Type (uint8)\n")
			fmt.Print("--code=<#>     Code Integer (uint8)\n")
			fmt.Print("--data=<#>     Data (uint64)\n")
			fmt.Print("--cksum=<#>    Message Checksum (uint16), if ommited checksum will be calculated automatically\n")
			fmt.Print("--ptr=<#>      Pointer (Type 12) (uint8)\n")
			fmt.Print("--gw=<A.B.C.D> Gateway Internet Address (Type 5) (uint32)\n")
			fmt.Print("--id=<#>       Identifier (Type 8,0,13,14,15,16) (uint16)\n")
			fmt.Print("--seq=<#>      Sequence Number (Type 8,0,13,14,15,16) (uint16)\n")
			fmt.Print("--org_t=<#>    Originate Timestamp (Type 13,14) (uint32)\n")
			fmt.Print("--rcv_t=<#>    Receive Timestamp (Type 13,14) (uint32)\n")
			fmt.Print("--trn_t=<#>    Transmit Timestamp (Type 13,14) (uint32)\n")
			fmt.Print("-q             Turn off verbose output\n")
			fmt.Print("W.X.Y.Z        Destination IP address\n")
			fmt.Print("--rfc          Print the packet structure per RFC\n")
			fmt.Print("--help         Print this help message\n")
			fmt.Print("\n\nReference:     RFC792\n")
			return
		}

		if arg == "--rfc" {
			fmt.Print("\n")
			fmt.Print("Destination Unreachable Message\n")
			fmt.Print("TYPE: 3\n")
			fmt.Print(" 0                   1                   2                   3\n")
			fmt.Print(" 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Type      |     Code      |          Checksum             |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|                             unused                            |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|      Internet Header + 64 bits of Original Data Datagram      |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("\nCODE:\n")
			fmt.Print("0 = net unreachable    1 = host unreachable                  2 = protocol unreachable\n")
			fmt.Print("3 = port unreachable   4 = fragmentation needed and DF set   5 = source route failed\n")

			fmt.Print("\n")
			fmt.Print("Time Exceeded Message\n")
			fmt.Print("TYPE: 11\n")
			fmt.Print(" 0                   1                   2                   3\n")
			fmt.Print(" 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Type      |     Code      |          Checksum             |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|                             unused                            |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|      Internet Header + 64 bits of Original Data Datagram      |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("\nCODE:\n")
			fmt.Print("0 = time to live exceeded in transit    1 = fragment reassembly time exceeded\n")

			fmt.Print("\n")
			fmt.Print("Parameter Problem Message\n")
			fmt.Print("TYPE: 12\n")
			fmt.Print(" 0                   1                   2                   3\n")
			fmt.Print(" 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Type      |     Code      |          Checksum             |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|    Pointer    |                   unused                      |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|      Internet Header + 64 bits of Original Data Datagram      |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("\nCODE:\n")
			fmt.Print("0 = pointer indicates the error\n")

			fmt.Print("\n")
			fmt.Print("Source Quench Message\n")
			fmt.Print("TYPE: 4\n")
			fmt.Print(" 0                   1                   2                   3\n")
			fmt.Print(" 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Type      |     Code      |          Checksum             |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|                             unused                            |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|      Internet Header + 64 bits of Original Data Datagram      |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("\nCODE:\n")
			fmt.Print("0\n")

			fmt.Print("\n")
			fmt.Print("Redirect Message\n")
			fmt.Print("TYPE: 5\n")
			fmt.Print(" 0                   1                   2                   3\n")
			fmt.Print(" 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Type      |     Code      |          Checksum             |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|                 Gateway Internet Address                      |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|      Internet Header + 64 bits of Original Data Datagram      |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("\nCODE:\n")
			fmt.Print("0 = Redirect datagrams for the Network                      1 = Redirect datagrams for the Host\n")
			fmt.Print("2 = Redirect datagrams for the Type of Service and Network  3 = Redirect datagrams for the Type of Service and Host\n")

			fmt.Print("\n")
			fmt.Print("Echo or Echo Reply Message\n")
			fmt.Print("TYPE: 8 (echo)   0 (echo reply)\n")
			fmt.Print(" 0                   1                   2                   3\n")
			fmt.Print(" 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Type      |     Code      |          Checksum             |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|           Identifier          |        Sequence Number        |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Data ...\n")
			fmt.Print("+-+-+-+-+-\n")
			fmt.Print("\nCODE:\n")
			fmt.Print("0\n")

			fmt.Print("\n")
			fmt.Print("Timestamp or Timestamp Reply Message\n")
			fmt.Print("TYPE: 13 (timestamp)    14 (timestamp reply)\n")
			fmt.Print(" 0                   1                   2                   3\n")
			fmt.Print(" 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Type      |      Code     |          Checksum             |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|           Identifier          |        Sequence Number        |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Originate Timestamp                                       |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Receive Timestamp                                         |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Transmit Timestamp                                        |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("\nCODE:\n")
			fmt.Print("0\n")

			fmt.Print("\n")
			fmt.Print("Information Request or Information Reply Message\n")
			fmt.Print("TYPE: 15 (information request)    16 (information reply)\n")
			fmt.Print(" 0                   1                   2                   3\n")
			fmt.Print(" 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|     Type      |      Code     |          Checksum             |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("|           Identifier          |        Sequence Number        |\n")
			fmt.Print("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n")
			fmt.Print("\nCODE:\n")
			fmt.Print("0\n")
		}

		if arg == "-q" {
			verbose = false
		}

		regex = regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+`)
		if regex.MatchString(arg) {
			ip = arg
		}

		regex = regexp.MustCompile(`--type=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 8)
			er(err)
			typ = byte(tmp)
		}

		regex = regexp.MustCompile(`--code=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 8)
			er(err)
			code = byte(tmp)
		}

		regex = regexp.MustCompile(`--data=(\d+)`)
		if regex.MatchString(arg) {
			data, err = strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 64)
			er(err)
		}

		regex = regexp.MustCompile(`--cksum=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 16)
			er(err)
			cksum = uint16(tmp)
		}

		regex = regexp.MustCompile(`--ptr=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 16)
			er(err)
			ptr = uint8(tmp)
		}

		regex = regexp.MustCompile(`--gw=(\d+)\.(\d+)\.(\d+)\.(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)
			gw[0] = byte(tmp)
			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[2])
			er(err)
			gw[1] = byte(tmp)
			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[3])
			er(err)
			gw[2] = byte(tmp)
			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[4])
			er(err)
			gw[3] = byte(tmp)

		}

		regex = regexp.MustCompile(`id=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 16)
			er(err)
			id = uint16(tmp)
		}

		regex = regexp.MustCompile(`seq=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 16)
			er(err)
			seq = uint16(tmp)
		}

		regex = regexp.MustCompile(`org_t=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 32)
			er(err)
			org_t = uint32(tmp)
		}
		regex = regexp.MustCompile(`rcv_t=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 32)
			er(err)
			rcv_t = uint32(tmp)
		}
		regex = regexp.MustCompile(`trn_t=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 32)
			er(err)
			trn_t = uint32(tmp)
		}

	}

	if ip == "" {
		er(fmt.Errorf("must specify destination IP address"))
	}

	if typ == 3 || typ == 11 || typ == 4 {
		var typeByte [1]byte
		var codeByte [1]byte
		var cksumByte [2]byte
		var unusedByte [4]byte
		var dataByte []byte

		typeByte[0] = byte(typ)
		codeByte[0] = byte(code)
		cksumByte[0] = byte(cksum >> 8)
		cksumByte[1] = byte(cksum << 8 >> 8)
		dataByte = calcData(data)

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], unusedByte[0], unusedByte[1], unusedByte[2], unusedByte[3])
		msg = append(msg, dataByte...)

		if cksum == 0 {
			cksumByte = checksum(msg)
		}

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], unusedByte[0], unusedByte[1], unusedByte[2], unusedByte[3])
		msg = append(msg, dataByte...)

		if verbose {
			printb(msg)
		}

		sendMsg(msg, ip)

	}

	if typ == 12 {
		var typeByte [1]byte
		var codeByte [1]byte
		var cksumByte [2]byte
		var ptrByte [1]byte
		var unusedByte [3]byte
		var dataByte []byte

		typeByte[0] = byte(typ)
		codeByte[0] = byte(code)
		cksumByte[0] = byte(cksum >> 8)
		cksumByte[1] = byte(cksum << 8 >> 8)
		ptrByte[0] = byte(ptr)
		dataByte = calcData(data)

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], ptrByte[0], unusedByte[0], unusedByte[1], unusedByte[2])
		msg = append(msg, dataByte...)

		if cksum == 0 {
			cksumByte = checksum(msg)
		}

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], ptrByte[0], unusedByte[0], unusedByte[1], unusedByte[2])
		msg = append(msg, dataByte...)

		if verbose {
			printb(msg)
		}

		sendMsg(msg, ip)
	}

	if typ == 5 {
		var typeByte [1]byte
		var codeByte [1]byte
		var cksumByte [2]byte
		var gwByte [4]byte
		var dataByte []byte

		typeByte[0] = byte(typ)
		codeByte[0] = byte(code)
		cksumByte[0] = byte(cksum >> 8)
		cksumByte[1] = byte(cksum << 8 >> 8)
		gwByte = gw
		dataByte = calcData(data)

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], gwByte[0], gwByte[1], gwByte[2], gwByte[3])
		msg = append(msg, dataByte...)

		if cksum == 0 {
			cksumByte = checksum(msg)
		}

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], gwByte[0], gwByte[1], gwByte[2], gwByte[3])
		msg = append(msg, dataByte...)

		if verbose {
			printb(msg)
		}

		sendMsg(msg, ip)
	}

	if typ == 8 || typ == 0 {
		var typeByte [1]byte
		var codeByte [1]byte
		var cksumByte [2]byte
		var idByte [2]byte
		var seqByte [2]byte
		var dataByte []byte

		typeByte[0] = byte(typ)
		codeByte[0] = byte(code)
		cksumByte[0] = byte(cksum >> 8)
		cksumByte[1] = byte(cksum << 8 >> 8)
		idByte[0] = byte(id >> 8)
		idByte[1] = byte(id << 8 >> 8)
		seqByte[0] = byte(seq >> 8)
		seqByte[1] = byte(seq << 8 >> 8)
		dataByte = calcData(data)

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], idByte[0], idByte[1], seqByte[0], seqByte[1])
		msg = append(msg, dataByte...)

		if cksum == 0 {
			cksumByte = checksum(msg)
		}

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], idByte[0], idByte[1], seqByte[0], seqByte[1])
		msg = append(msg, dataByte...)

		if verbose {
			printb(msg)
		}

		sendMsg(msg, ip)
	}

	if typ == 13 || typ == 14 {
		var typeByte [1]byte
		var codeByte [1]byte
		var cksumByte [2]byte
		var idByte [2]byte
		var seqByte [2]byte
		var org_tByte [4]byte
		var rcv_tByte [4]byte
		var trn_tByte [4]byte

		typeByte[0] = byte(typ)
		codeByte[0] = byte(code)
		cksumByte[0] = byte(cksum >> 8)
		cksumByte[1] = byte(cksum << 8 >> 8)
		idByte[0] = byte(id >> 8)
		idByte[1] = byte(id << 8 >> 8)
		seqByte[0] = byte(seq >> 8)
		seqByte[1] = byte(seq << 8 >> 8)
		org_tByte[0] = byte(org_t >> 24)
		org_tByte[1] = byte(org_t << 8 >> 24)
		org_tByte[2] = byte(org_t << 16 >> 24)
		org_tByte[3] = byte(org_t << 24 >> 24)
		rcv_tByte[0] = byte(rcv_t >> 24)
		rcv_tByte[1] = byte(rcv_t << 8 >> 24)
		rcv_tByte[2] = byte(rcv_t << 16 >> 24)
		rcv_tByte[3] = byte(rcv_t << 24 >> 24)
		trn_tByte[0] = byte(trn_t >> 24)
		trn_tByte[1] = byte(trn_t << 8 >> 24)
		trn_tByte[2] = byte(trn_t << 16 >> 24)
		trn_tByte[3] = byte(trn_t << 24 >> 24)

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], idByte[0], idByte[1], seqByte[0], seqByte[1])
		msg = append(msg, org_tByte[0], org_tByte[1], org_tByte[2], org_tByte[3], rcv_tByte[0], rcv_tByte[1], rcv_tByte[2], rcv_tByte[3], trn_tByte[0], trn_tByte[1], trn_tByte[2], trn_tByte[3])

		if cksum == 0 {
			cksumByte = checksum(msg)
		}

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], idByte[0], idByte[1], seqByte[0], seqByte[1])
		msg = append(msg, org_tByte[0], org_tByte[1], org_tByte[2], org_tByte[3], rcv_tByte[0], rcv_tByte[1], rcv_tByte[2], rcv_tByte[3], trn_tByte[0], trn_tByte[1], trn_tByte[2], trn_tByte[3])

		if verbose {
			printb(msg)
		}

		sendMsg(msg, ip)
	}

	if typ == 15 || typ == 16 {
		var typeByte [1]byte
		var codeByte [1]byte
		var cksumByte [2]byte
		var idByte [2]byte
		var seqByte [2]byte

		typeByte[0] = byte(typ)
		codeByte[0] = byte(code)
		cksumByte[0] = byte(cksum >> 8)
		cksumByte[1] = byte(cksum << 8 >> 8)
		idByte[0] = byte(id >> 8)
		idByte[1] = byte(id << 8 >> 8)
		seqByte[0] = byte(seq >> 8)
		seqByte[1] = byte(seq << 8 >> 8)

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], idByte[0], idByte[1], seqByte[0], seqByte[1])

		if cksum == 0 {
			cksumByte = checksum(msg)
		}

		msg = nil
		msg = append(msg, typeByte[0], codeByte[0], cksumByte[0], cksumByte[1], idByte[0], idByte[1], seqByte[0], seqByte[1])

		if verbose {
			printb(msg)
		}

		sendMsg(msg, ip)
	}
}

func calcData(data uint64) []byte {
	var dataByte []byte

	if data != 0 {
		dataByte = append(dataByte, byte(data))
	}

	if data>>8 != 0 {
		dataByte = append(dataByte, byte(data>>8))
	}

	if data>>16 != 0 {
		dataByte = append(dataByte, byte(data>>16))
	}

	if data>>24 != 0 {
		dataByte = append(dataByte, byte(data>>16))
	}

	if data>>32 != 0 {
		dataByte = append(dataByte, byte(data>>32))
	}

	if data>>40 != 0 {
		dataByte = append(dataByte, byte(data>>40))
	}

	if data>>48 != 0 {
		dataByte = append(dataByte, byte(data>>48))
	}

	if data>>56 != 0 {
		dataByte = append(dataByte, byte(data>>56))
	}

	return dataByte
}

func checksum(msg []byte) [2]byte {
	var tmp uint32

	if len(msg)%2 != 0 {
		msg = append(msg, 0)
	}

	for i := 0; i < len(msg); i++ {
		if i%2 != 0 {
			tmp += (uint32(msg[i-1]))<<8 + uint32(msg[i])
			tmp = (tmp & 0xFFFF) + (tmp >> 16)
		}
	}

	var cksum [2]byte
	cksum[0] = byte(tmp >> 8)
	cksum[1] = byte(tmp << 8 >> 8)
	cksum[0] = cksum[0] ^ 0xFF
	cksum[1] = cksum[1] ^ 0xFF

	return cksum
}

func sendMsg(msg []byte, ip string) {
	conn, err := net.Dial("ip4:1", ip)
	er(err)
	defer conn.Close()

	_, err = conn.Write(msg)
	er(err)

}

func printb(b []byte) {
	fmt.Print("\n")
	for i := 0; i < len(b); i++ {
		if i%4 == 0 {
			var byte1, byte2, byte3, byte4 byte
			byte1 = b[i]
			if i+1 < len(b) {
				byte2 = b[i+1]
			}
			if i+2 < len(b) {
				byte3 = b[i+2]
			}
			if i+3 < len(b) {
				byte4 = b[i+3]
			}
			fmt.Printf("  %08b  %08b  %08b  %08b    %02X  %02X  %02X  %02X    %s  %s  %s  %s\n", byte1, byte2, byte3, byte4, byte1, byte2, byte3, byte4, string(byte1), string(byte2), string(byte3), string(byte4))
		}
	}
}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		if len(cleanup) > 0 {
			if cleanup[0] != "" {
				_, err := os.Stat(cleanup[0])
				er(err, "")
				err = os.Remove(cleanup[0])
				er(err, "")
			}
		}
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
