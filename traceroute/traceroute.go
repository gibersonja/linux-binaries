package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
)

/*
var maxTTL int = 30
var gateways []string
*/
var icmp bool = true

/*var maxHops int = 64
var typeMethod bool // false = UDP, true = ICMP
var port uint16 = 33434
var tries int = 3
var resolveHostnames bool
var tos int
var wait int = 3 */

var icmpPacket []byte
var destIP string

func main() {
	if len(os.Args) != 1 {
		args := os.Args[1:]
		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" || arg == "-?" {
				fmt.Print("Usage: traceroute [OPTION...] HOST\n")
				fmt.Print("Print the route packets trace to network host.\n")
				fmt.Print("\n")
				fmt.Print("  -f, --first-hop=NUM        set initial hop distance, i.e., time-to-live\n")
				fmt.Print("  -g, --gateways=GATES       list of gateways for loose source routing\n")
				fmt.Print("  -I, --icmp                 use ICMP ECHO as probe\n")
				fmt.Print("  -m, --max-hop=NUM          set maximal hop count (default: 64)\n")
				fmt.Print("  -M, --type=METHOD          use METHOD (`icmp' or `udp') for traceroute\n")
				fmt.Print("                             operations, defaulting to `udp'\n")
				fmt.Print("  -p, --port=PORT            use destination PORT port (default: 33434)\n")
				fmt.Print("  -q, --tries=NUM            send NUM probe packets per hop (default: 3)\n")
				fmt.Print("      --resolve-hostnames    resolve hostnames\n")
				fmt.Print("  -t, --tos=NUM              set type of service (TOS) to NUM\n")
				fmt.Print("  -w, --wait=NUM             wait NUM seconds for response (default: 3)\n")
				fmt.Print("  -?, --help                 give this help list\n")
				fmt.Print("      --usage                give a short usage message\n")
				fmt.Print("  -V, --version              print program version\n")
				fmt.Print("\n")
				fmt.Print("Mandatory or optional arguments to long options are also mandatory or optional\n")
				fmt.Print("for any corresponding short options.\n")

				return
			}

			regexIP := regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+$`)

			if regexIP.MatchString(arg) {
				destIP = arg
			}
		}
	}

	if destIP == "" {
		er(fmt.Errorf("must enter a valid IP address"))
	}

	if icmp {
		icmpPacket = append(icmpPacket, 8)            // Set Type       = 8 (echo)
		icmpPacket = append(icmpPacket, 0)            // Set Code Byte  = 0
		icmpPacket = append(icmpPacket, 0, 0)         // Set Checksum   = 0
		icmpPacket = append(icmpPacket, 0, 0)         // Set ID Bytes   = 0
		icmpPacket = append(icmpPacket, 0, 0)         // Set Seq Bytes  = 0
		icmpPacket = append(icmpPacket, 0, 13, 37, 0) // Set Data Bytes = 0x0013 0x3700

		cksum := checksum(icmpPacket)
		icmpPacket[2] = cksum[0]
		icmpPacket[3] = cksum[1]

		getMsg := sendMsg(icmpPacket, destIP)
		for j := 0; j < len(getMsg); j++ {
			fmt.Printf("%04d  %03d %02X %08b\n", j+1, getMsg[j], getMsg[j], getMsg[j])
		}
	}
}

func sendMsg(msg []byte, ip string) []byte {

	conn, err := net.Dial("ip4:1", ip)
	er(err)
	defer conn.Close()

	_, err = conn.Write(msg)
	er(err)

	returnBuffer := make([]byte, 4500)
	_, err = conn.Read(returnBuffer)
	er(err)

	headerLength := uint16(returnBuffer[0]<<4>>4) * 4                       // Header length in 32 bit words
	totalLength := (uint16(returnBuffer[2]) << 8) + uint16(returnBuffer[3]) // Total lneght including header
	return returnBuffer[headerLength:totalLength]                           // Only return the ICMP part of the buffer
}

func checksum(msg []byte) [2]byte {
	var tmp uint32

	if len(msg)%2 != 0 {
		msg = append(msg, 0)
	}

	for i := 0; i < len(msg); i++ {
		if i%2 != 0 {
			tmp += (uint32(msg[i-1]))<<8 + uint32(msg[i])
			tmp = (tmp & 0xFFFF) + (tmp >> 16)
		}
	}

	var cksum [2]byte
	cksum[0] = byte(tmp >> 8)
	cksum[1] = byte(tmp << 8 >> 8)
	cksum[0] = cksum[0] ^ 0xFF
	cksum[1] = cksum[1] ^ 0xFF

	return cksum
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
