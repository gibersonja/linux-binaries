package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
)

var board [9][9]rune

func main() {
	for i := 0; i < len(board); i++ {
		for j := 0; j < len(board[i]); j++ {
			board[i][j] = ' '
		}
	}

	defaultGame()
	printBoard()
	rowSolve()
	colSolve()
}

func printBoard() {
	for i := 0; i < 30; i++ {
		fmt.Print("\n")
	}
	for y := 0; y < len(board); y++ {
		if y == 0 {
			fmt.Print("    1   2   3   4   5   6   7   8   9\n")
			fmt.Print("  ╭───┬───┬───┰───┬───┬───┰───┬───┬───╮\n")
		}

		fmt.Printf("%d ", y+1)

		for x := 0; x < len(board[y]); x++ {
			if x == 0 {
				fmt.Print("│")
			}

			if x == 2 || x == 5 {
				fmt.Printf(" %c ┃", board[y][x])
			} else {
				fmt.Printf(" %c │", board[y][x])
			}
			if x == len(board[y])-1 {
				fmt.Printf(" %d\n", y+1)
			}
		}

		if y == len(board)-1 {
			fmt.Print("  ╰───┴───┴───┸───┴───┴───┸───┴───┴───╯\n")
			fmt.Print("    1   2   3   4   5   6   7   8   9\n")
		} else {
			if y == 2 || y == 5 {
				fmt.Print("  ┝━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┥\n")
			} else {
				fmt.Print("  ├───┼───┼───╂───┼───┼───╂───┼───┼───┤\n")
			}
		}
	}
}

func colSolve() {
	colCount, colSum, colX, colY := 0, 0, 0, 0

	for x := 0; x < 9; x++ {
		for y := 0; y < 9; y++ {
			if board[y][x] == ' ' {
				colCount++
				colX, colY = x, y
			} else {
				colSum += (int(board[y][x]) - 48)
			}
		}

		if colCount == 1 {
			board[colY][colX] = rune(93 - colSum) // because 45 + 48 = 93
			printBoard()
		}
	}

}

func rowSolve() {
	for y := 0; y < len(board); y++ {
		rowCount, rowSum, rowX, rowY := 0, 0, 0, 0

		for x := 0; x < len(board[y]); x++ {
			if board[y][x] == ' ' {
				rowCount++
				rowX, rowY = x, y
			} else {
				rowSum += (int(board[y][x]) - 48)
			}
		}

		if rowCount == 1 {
			//board[rowY][rowX] = rune(45 - rowSum + 48) Therefore:
			board[rowY][rowX] = rune(93 - rowSum)
			printBoard()
		}
	}
}

func squareSolve() {

}

func defaultGame() {
	// From wikipedia: https://en.wikipedia.org/wiki/Sudoku

	board[0][0] = '5'
	board[0][1] = '3'
	board[0][4] = '7'

	board[1][0] = '6'
	board[1][3] = '1'
	board[1][4] = '9'
	board[1][5] = '5'

	board[2][1] = '9'
	board[2][2] = '8'
	board[2][7] = '6'

	board[3][0] = '8'
	board[3][4] = '6'
	board[3][8] = '3'

	board[4][0] = '4'
	board[4][3] = '8'
	board[4][5] = '3'
	board[4][8] = '1'

	board[5][0] = '7'
	board[5][4] = '2'
	board[5][8] = '6'

	board[6][1] = '6'
	board[6][6] = '2'
	board[6][7] = '8'

	board[7][3] = '4'
	board[7][4] = '1'
	board[7][5] = '9'
	board[7][8] = '5'

	board[8][4] = '8'
	board[8][7] = '7'
	board[8][8] = '9'

	// additional solved fields

	// horizontal
	board[0][2] = '4'
	board[0][3] = '6'
	board[0][6] = '9'
	board[0][7] = '1'
	board[0][8] = '2'

	// vertical
	board[1][8] = '8'
	board[2][8] = '7'

	// square
	board[1][6] = '3'
	board[1][7] = '4'

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
