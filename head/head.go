package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
)

var data []byte
var lines int = 10 // need to check if this is negative, if so print all but last n lines
var bytes int      // if negative print all but last n bytes
var err error
var nullTerm bool

func main() {
	if len(os.Args) != 1 {
		args := os.Args[1:]

		regexLines := regexp.MustCompile(`^--lines=(-?\d+)$`)
		regexBytes := regexp.MustCompile(`^--bytes=(-?\d+)$`)

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("--help   Print this help message\n")

				return
			} else if arg == "-n" || regexLines.MatchString(arg) {
				if arg == "-n" && i+1 < len(args) {
					lines, err = strconv.Atoi(args[i+1])
					er(err)
				} else {
					regexMatch := regexLines.FindStringSubmatch(arg)
					lines, err = strconv.Atoi(regexMatch[1])
					er(err)
				}
				i++
			} else if arg == "-c" || regexBytes.MatchString(arg) {
				if arg == "-c" && i+1 < len(args) {
					bytes, err = strconv.Atoi(args[i+1])
					er(err)
				} else {
					regexMatch := regexBytes.FindStringSubmatch(arg)
					bytes, err = strconv.Atoi(regexMatch[i])
					er(err)
				}
				i++
			} else if arg == "-z" || arg == "--zero-terminated" {
				nullTerm = true
			} else { // add support for filename globbing
				fhInfo, err := os.Stat(arg)
				er(err)

				fileName := arg
				fileSize := fhInfo.Size()

				fh, err := os.Open(fileName)
				er(err)
				defer fh.Close()

				fileBuffer := make([]byte, fileSize)
				_, err = bufio.NewReader(fh).Read(fileBuffer)
				if err != nil && err != io.EOF {
					er(err)
				}

				data = fileBuffer
			}
		}
	}

	var lf byte = 10
	var stringData []string
	var start int
	var stop int

	if nullTerm {
		lf = 0
	}

	if bytes == 0 {
		for i := 0; i < len(data); i++ {
			if data[i] == lf {
				stop = i
				stringData = append(stringData, string(data[start:stop]))
				if i+1 < len(data) {
					start = i + 1
				}
			}
		}

		var linesOutput []string
		if lines < 0 && -1*lines < len(stringData) {
			linesOutput = stringData[:(len(stringData) + lines)]
		}

		if lines > 0 && lines < len(stringData) {
			linesOutput = stringData[:lines]
		}

		for i := 0; i < len(linesOutput); i++ {
			fmt.Printf("%s\n", linesOutput[i])
		}
	} else {
		var bytesOutput []byte
		if bytes < 0 && -1*bytes < len(data) {
			bytesOutput = data[:(len(data) + bytes)]
		}

		if bytes > 0 && bytes < len(data) {
			bytesOutput = data[:bytes]
		}

		for i := 0; i < len(bytesOutput); i++ {
			fmt.Printf("%c", bytesOutput[i])
		}
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
