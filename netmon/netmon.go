package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/guptarohit/asciigraph"
)

var ips []string
var response_time = make(map[string][]float64)
var timeout int = 3000
var wait_time int = 500
var graphWidth int = 100
var graphHeight int = 25
var graphPrecision uint = 2
var graph bool = true
var color bool = true
var err error

func main() {
	regexIP := regexp.MustCompile(`\d+\.\d+\.\d+\.\d+`)
	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "-h" || arg == "--help" {
				fmt.Print("-h | --help      Print this help message\n")
				fmt.Print("-t | --timeout   Specify ICMP timout in ms\n")
				fmt.Print("-w | --wait      Specify time to wait between ICMP packet per host\n")
				fmt.Print("-g | --graph     Disable graph display\n")
				fmt.Printf("-H | --height    Graph height, Default: %d\n", graphHeight)
				fmt.Printf("-W | --width     Graph width, Default: %d\n", graphWidth)
				fmt.Printf("-P | --precision Graph precision, Default: %d\n", graphPrecision)
				fmt.Printf("-c | --color     Turn off color, suggested for non ANSI terminal\n")

				return
			}

			if (arg == "-t" || arg == "--timeout") && i+1 < len(args) {
				timeout, err = strconv.Atoi(args[i+1])
				er(err)
			}

			if arg == "-c" || arg == "--color" {
				color = false
			}

			if (arg == "-w" || arg == "--wait") && i+1 < len(args) {
				wait_time, err = strconv.Atoi(args[i+1])
				er(err)
			}

			if arg == "--graph" || arg == "-g" {
				graph = false
			}

			if (arg == "-H" || arg == "--height") && i+1 < len(args) {
				graphHeight, err = strconv.Atoi(args[i+1])
				er(err)
			}

			if (arg == "-W" || arg == "--width") && i+1 < len(args) {
				graphWidth, err = strconv.Atoi(args[i+1])
				er(err)
			}

			if (arg == "-P" || arg == "--precision") && i+1 < len(args) {
				tmp, err := strconv.ParseUint(args[i+1], 10, 64)
				er(err)
				graphPrecision = uint(tmp)
			}

			if regexIP.MatchString(arg) {
				ips = append(ips, arg)
			}
		}
	}

	if color {
		fmt.Print("\033[H\033[2J")
	} else {
		var cmd *exec.Cmd

		if runtime.GOOS == "windows" {
			cmd = exec.Command("cmd", "/c", "cls")
			er(cmd.Err)
		} else {
			cmd = exec.Command("clear")
			er(cmd.Err)
		}

		cmd.Stdout = os.Stdout
		err := cmd.Run()
		er(err)
	}

	for {
		var data [][]float64
		fmt.Print("╭──────────────┬────────────┬───────────┬───────╮\n")
		fmt.Printf("│     HOST     │    LAST    │    AVG    │ COUNT │\n")
		fmt.Print("╰──────────────┴────────────┴───────────┴───────╯\n")
		for i := 0; i < len(ips); i++ {
			response_time[ips[i]] = append(response_time[ips[i]], icmp(ips[i]))
			response_avg := avg(response_time[ips[i]])
			fmt.Printf(" %s\t%.2fms\t\t%.2fms\t   %d\n", ips[i], response_time[ips[i]][len(response_time[ips[i]])-1], response_avg, len(response_time[ips[i]]))
			if graph {
				data = append(data, response_time[ips[i]])
			}
		}

		if graph {
			var plot string
			if len(data) >= 8 && color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.SeriesColors(asciigraph.Red, asciigraph.Blue, asciigraph.Green, asciigraph.Yellow, asciigraph.Purple, asciigraph.Silver, asciigraph.Magenta, asciigraph.Cyan),
					asciigraph.SeriesLegends(ips[0], ips[1], ips[2], ips[3], ips[4], ips[5], ips[6], ips[7]),
					asciigraph.Caption("Legend"),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			if len(data) == 7 && color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.SeriesColors(asciigraph.Red, asciigraph.Blue, asciigraph.Green, asciigraph.Yellow, asciigraph.Purple, asciigraph.Silver, asciigraph.Magenta),
					asciigraph.SeriesLegends(ips[0], ips[1], ips[2], ips[3], ips[4], ips[5], ips[6]),
					asciigraph.Caption("Legend"),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			if len(data) == 6 && color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.SeriesColors(asciigraph.Red, asciigraph.Blue, asciigraph.Green, asciigraph.Yellow, asciigraph.Purple, asciigraph.Silver),
					asciigraph.SeriesLegends(ips[0], ips[1], ips[2], ips[3], ips[4], ips[5]),
					asciigraph.Caption("Legend"),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			if len(data) == 5 && color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.SeriesColors(asciigraph.Red, asciigraph.Blue, asciigraph.Green, asciigraph.Yellow, asciigraph.Purple),
					asciigraph.SeriesLegends(ips[0], ips[1], ips[2], ips[3], ips[4]),
					asciigraph.Caption("Legend"),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			if len(data) == 4 && color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.SeriesColors(asciigraph.Red, asciigraph.Blue, asciigraph.Green, asciigraph.Yellow),
					asciigraph.SeriesLegends(ips[0], ips[1], ips[2], ips[3]),
					asciigraph.Caption("Legend"),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			if len(data) == 3 && color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.SeriesColors(asciigraph.Red, asciigraph.Blue, asciigraph.Green),
					asciigraph.SeriesLegends(ips[0], ips[1], ips[2]),
					asciigraph.Caption("Legend"),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			if len(data) == 2 && color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.SeriesColors(asciigraph.Red, asciigraph.Blue),
					asciigraph.SeriesLegends(ips[0], ips[1]),
					asciigraph.Caption("Legend"),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			if len(data) == 1 && color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.SeriesColors(asciigraph.Red),
					asciigraph.SeriesLegends(ips[0]),
					asciigraph.Caption("Legend"),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			if !color {
				plot = asciigraph.PlotMany(data,
					asciigraph.Precision(graphPrecision),
					asciigraph.Width(graphWidth),
					asciigraph.Height(graphHeight))
			}

			plot = strings.ReplaceAll(plot, "\n", "\n│")
			plot = "│" + plot

			fmt.Print("\n╭")
			for i := 0; i < graphWidth+4+int(graphPrecision); i++ {
				fmt.Print("─")
			}
			fmt.Print("╮\n")
			fmt.Print(plot)
			fmt.Print("\n╰")
			for i := 0; i < graphWidth+4+int(graphPrecision); i++ {
				fmt.Print("─")
			}
			fmt.Print("╯\n")

		}

		time.Sleep(time.Duration(wait_time) * time.Millisecond)

		if color {
			fmt.Print("\033[H\033[2J")
		} else {
			var cmd *exec.Cmd

			if runtime.GOOS == "windows" {
				cmd = exec.Command("cmd", "/c", "cls")
				er(cmd.Err)
			} else {
				cmd = exec.Command("clear")
				er(cmd.Err)
			}

			cmd.Stdout = os.Stdout
			err := cmd.Run()
			er(err)
		}
	}
}

func avg(nums []float64) float64 {
	var tmp float64
	for i := 0; i < len(nums); i++ {
		tmp += nums[i]
	}

	return tmp / float64(len(nums))
}

func icmp(addr string) float64 {
	var icmpPacket []byte

	icmpPacket = append(icmpPacket, 8, 0, 0, 0, 0, 0, 0, 0, 0x13, 0x37, 0x13, 0x37, 0x13, 0x37)
	cksum := checksum(icmpPacket)
	icmpPacket[2] = cksum[0]
	icmpPacket[3] = cksum[1]

	conn, err := net.Dial("ip4:1", addr)
	er(err)
	defer conn.Close()
	conn.SetDeadline(time.Now().Add(time.Duration(timeout) * time.Millisecond))

	returnBuffer := make([]byte, 4500)

	startTime := time.Now()
	_, err = conn.Write(icmpPacket)
	_, err = conn.Read(returnBuffer)
	stopTime := time.Now()

	return float64(time.Time.Sub(stopTime, startTime).Nanoseconds()) / 1000000
}

func checksum(msg []byte) [2]byte {
	var tmp uint32

	if len(msg)%2 != 0 {
		msg = append(msg, 0)
	}

	for i := 0; i < len(msg); i++ {
		if i%2 != 0 {
			tmp += (uint32(msg[i-1]))<<8 + uint32(msg[i])
			tmp = (tmp & 0xFFFF) + (tmp >> 16)
		}
	}

	var cksum [2]byte
	cksum[0] = byte(tmp >> 8)
	cksum[1] = byte(tmp << 8 >> 8)
	cksum[0] = cksum[0] ^ 0xFF
	cksum[1] = cksum[1] ^ 0xFF

	return cksum
}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
