package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"runtime"
)

var data []byte

func main() {

	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			data = append(data, scanner.Bytes()...)
		}

		err := scanner.Err()
		er(err)
	}

	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "-h" || arg == "--help" {
				fmt.Print("-h | --help  Print this help message\n")

				return
			} else {
				fhInfo, err := os.Stat(arg)
				er(err)

				fileName := arg
				fileSize := fhInfo.Size()

				fh, err := os.Open(fileName)
				er(err)
				defer fh.Close()

				fileBuf := make([]byte, fileSize)
				_, err = bufio.NewReader(fh).Read(fileBuf)
				if err != nil && err != io.EOF {
					er(err)
				}

				data = fileBuf
			}
		}

	}

	// CRLF = Windows
	// LF = Unix
	// CR = Mac

	/* Things to add from man page:

							 -b, --body-numbering=STYLE
	              use STYLE for numbering body lines

	       -d, --section-delimiter=CC
	              use CC for logical page delimiters

	       -f, --footer-numbering=STYLE
	              use STYLE for numbering footer lines

	       -h, --header-numbering=STYLE
	              use STYLE for numbering header lines

	       -i, --line-increment=NUMBER
	              line number increment at each line

	       -l, --join-blank-lines=NUMBER
	              group of NUMBER empty lines counted as one

	       -n, --number-format=FORMAT
	              insert line numbers according to FORMAT

	       -p, --no-renumber
	              do not reset line numbers for each section

	       -s, --number-separator=STRING
	              add STRING after (possible) line number

	       -v, --starting-line-number=NUMBER
	              first line number for each section

	       -w, --number-width=NUMBER
	              use NUMBER columns for line numbers

	       --help display this help and exit

			--version
					output version information and exit

			Default options are: -bt -d'\:' -fn -hn -i1 -l1 -n'rn' -s<TAB> -v1 -w6

			CC are two delimiter characters used to construct logical page  delim‐
			iters; a missing second character implies ':'.

			STYLE is one of:

			a      number all lines

			t      number only nonempty lines

			n      number no lines

			pBRE   number  only  lines  that contain a match for the basic regular
					expression, BRE

			FORMAT is one of:

			ln     left justified, no leading zeros

			rn     right justified, no leading zeros

			rz     right justified, leading zeros

	*/

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
