package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"runtime"
	"time"
)

func main() {

	if len(os.Args) != 1 {

		args := os.Args[1:]
		binary, err := exec.LookPath(args[0])
		er(err)

		cmd := exec.Command(binary, args[1:]...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		timeStart := time.Now()
		err = cmd.Run()
		timeStop := time.Now()
		er(err)

		timeDelta := time.Time.Sub(timeStop, timeStart)
		fmt.Println(timeDelta)

	} else {
		er(fmt.Errorf("no arguments given"))
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
