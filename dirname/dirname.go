package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
)

var fileNames []string
var zero bool

func main() {
	if len(os.Args) != 1 {
		args := os.Args[1:]
		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("-z, --zero\n")
				fmt.Print("       end each output line with NUL, not newline\n\n")
				fmt.Print("--help display this help and exit\n")

				return
			} else if arg == "-z" || arg == "--zero" {
				zero = true
			} else {
				fileNames = append(fileNames, arg)
			}
		}
	}

	if len(fileNames) == 0 {
		er(fmt.Errorf("missing operand"))
	}

	for i := 0; i < len(fileNames); i++ {
		getDirname(fileNames[i])
	}
}

func getDirname(filename string) {
	regex := regexp.MustCompile(`^(.*)[/][^/]+$`)
	if len(regex.FindStringSubmatch(filename)) > 1 {
		filename = regex.FindStringSubmatch(filename)[1]
	}

	if !zero {
		fmt.Printf("%s\n", filename)
	} else {
		fmt.Printf("%s%c", filename, 0)
	}

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
