package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
)

var initial bool
var tabsNum int = 7
var tabsString []string
var files []string
var err error
var output []byte

var tabsNumRegex *regexp.Regexp = regexp.MustCompile(`^--tabs=(\d+)$`)
var tabsStringRegex *regexp.Regexp = regexp.MustCompile(`^--tabs=(\S+)$`)

func main() {
	if len(os.Args) != 1 {
		args := os.Args[1:]
		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("-i, --initial\n")
				fmt.Print("       do not convert tabs after non blank lines\n\n")
				fmt.Print("-t, --tabs=N\n")
				fmt.Print("       have tabs N characters apart, not 8\n\n")
				fmt.Print("-t, --tabs=LIST\n")
				fmt.Print("       use comma separated list of tab positions The las specified position can be prefixed with '/' to specify a tab size to use after the last explicitly specifed tab stop.  Also a prefix of '+' can be used to align remaining tab stops relative to the last specified tab stop instead of the first column\n\n")
				fmt.Print("--help display this help and exit\n")

				return
			} else if arg == "-i" || arg == "--initial" {
				initial = true
			} else if arg == "-t" && i+1 < len(args) {
				regex := regexp.MustCompile(`^\d+$`)
				if regex.MatchString(args[i+1]) {
					tabsNum, err = strconv.Atoi(args[i+1])
					er(err)

					i++
				}
			} else {
				files = append(files, arg)
			}
		}
	}

	for i := 0; i < len(files); i++ {
		filename := files[i]
		fhInfo, err := os.Stat(filename)
		er(err)

		fileSize := fhInfo.Size()

		fh, err := os.Open(filename)
		er(err)
		defer fh.Close()

		fileBuffer := make([]byte, fileSize)
		_, err = bufio.NewReader(fh).Read(fileBuffer)
		if err != nil && err != io.EOF {
			er(err)
		}

		lenBuffer := len(fileBuffer)
		for j := 0; j < lenBuffer; j++ {
			// TAB == 9 && SPACE == 32
			if fileBuffer[j] == 9 {
				for k := 0; k < tabsNum; k++ {
					output = append(output, 32)
				}
			} else {
				output = append(output, fileBuffer[j])
			}
		}
	}
	for i := 0; i < len(output); i++ {
		fmt.Printf("%c", output[i])
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
