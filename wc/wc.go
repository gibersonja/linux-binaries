package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
	"strings"
)

var data []byte
var onlyBytes bool
var onlyChars bool
var onlyLines bool
var onlyWords bool
var onlyMaxLines bool
var onlyFiles0 bool
var fileName0 string
var fileName string
var nullFileArgNum int

func main() {
	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			data = append(data, scanner.Bytes()...)
		}

		err := scanner.Err()
		er(err)
	}

	if len(os.Args) != 1 {
		args := os.Args[1:]

		regex := regexp.MustCompile(`^--files0-from=(\S+)$`)

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("-c, --bytes\n")
				fmt.Print("       print the byte counts\n\n")
				fmt.Print("-m, --chars\n")
				fmt.Print("       print the character counts\n\n")
				fmt.Print("-l, --lines\n")
				fmt.Print("       print the newline counts\n\n")
				fmt.Print("-L, --max-line-length\n")
				fmt.Print("       print the maximum display width\n\n")
				fmt.Print("-w, --words\n")
				fmt.Print("       print the word counts\n\n")
				fmt.Print("--help display this help message\n")

				return
			} else if arg == "-c" || arg == "--bytes" {
				onlyBytes = true
			} else if arg == "-l" || arg == "--lines" {
				onlyLines = true
			} else if arg == "-w" || arg == "--words" {
				onlyWords = true
			} else if arg == "-L" || arg == "--max-line-length" {
				onlyMaxLines = true
			} else if regex.MatchString(arg) {
				matchString := regex.FindStringSubmatch(arg)
				fileName0 = matchString[1]
				onlyFiles0 = true
				nullFileArgNum = i + 1
			} else { // add support for filename globbing
				fhInfo, err := os.Stat(arg)
				er(err)

				fileName = arg
				fileSize := fhInfo.Size()

				fh, err := os.Open(fileName)
				er(err)
				defer fh.Close()

				fileBuffer := make([]byte, fileSize)
				_, err = bufio.NewReader(fh).Read(fileBuffer)
				if err != nil && err != io.EOF {
					er(err)
				}

				data = fileBuffer
			}
		}
	}

	var cr byte = 13
	var lf byte = 10
	var space byte = 32
	var tab byte = 9
	var stringData []string
	var start int
	var stop int
	var newLines int
	var words int
	var bytes int
	var chars int
	var maxLine int

	for i := 0; i < len(data); i++ {
		bytes++
		if i-1 >= 0 && ((data[i-1] == cr && data[i] == lf) || (data[i-1] == cr && data[i] != lf) || (data[i-1] != cr && data[i] == lf)) {
			stop = i + 1
			stringData = append(stringData, string(data[start:stop]))
			start = stop
		}
	}

	for i := 0; i < len(stringData); i++ {
		newLines++
		chars += len(stringData) // <-- not working right

		if i == 0 {
			maxLine = len(strings.ReplaceAll(strings.ReplaceAll(stringData[i], string(cr), ""), string(lf), ""))
		} else {
			if len(strings.ReplaceAll(strings.ReplaceAll(stringData[i], string(cr), ""), string(lf), "")) > maxLine {
				maxLine = len(strings.ReplaceAll(strings.ReplaceAll(stringData[i], string(cr), ""), string(lf), ""))
			}
		}
	}

	for i := 0; i < len(data); i++ {
		if (data[i] == space || data[i] == tab || data[i] == cr || data[i] == lf) && i > 0 && data[i-1] != space && data[i-1] != tab && data[i-1] != cr && data[i-1] != lf {
			words++
		}
	}

	if onlyBytes {
		fmt.Printf("%d ", bytes)
	} else if onlyWords {
		fmt.Printf("%d ", words)
	} else if onlyLines {
		fmt.Printf("%d ", newLines)
	} else if onlyMaxLines {
		fmt.Printf("%d ", maxLine)
	} else if onlyFiles0 {
		if fileName != "" {
			er(fmt.Errorf("file operands cannot be combined with --files0-from"))
		}

		fhInfo, err := os.Stat(fileName0)
		er(err)

		fileSize := fhInfo.Size()

		fh, err := os.Open(fileName0)
		er(err)
		defer fh.Close()

		fileBuffer := make([]byte, fileSize)
		_, err = bufio.NewReader(fh).Read(fileBuffer)
		if err != nil && err != io.EOF {
			er(err)
		}

		var fileList []string
		nullFileList := fileBuffer
		start := 0
		stop := 0

		for j := 0; j < len(nullFileList); j++ {
			if nullFileList[j] == 0 {
				stop = j
				fileList = append(fileList, string(nullFileList[start:stop]))
				if j+1 < len(nullFileList) {
					start = j + 1
				}
			}
		}

		onlyFiles0 = false

		for j := 0; j < len(fileList); j++ {
			os.Args[nullFileArgNum] = fileList[j]
			main()
		}

		fileName = ""

	} else {
		fmt.Printf(" %d  %d %d ", newLines, words, bytes)
	}
	if fileName != "" {
		fmt.Printf("%s", fileName)
	}

	fmt.Printf("\n")
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
