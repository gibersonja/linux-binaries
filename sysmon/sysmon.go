package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"time"

	"github.com/guptarohit/asciigraph"
)

var cpuFile string = "/proc/loadavg"
var memFile string = "/proc/meminfo"
var wait_time int = 300
var graphPrecision uint = 2
var graphWidth int = 100
var graphHeight int = 10
var dataPoints int = 50
var units string = "KB"
var icmpCheck bool
var addr1 string = "8.8.8.8"        // Google
var addr2 string = "208.67.222.222" // OpenDNS
var addr3 string = "1.1.1.1"        // Cloudflare
var err error

func main() {
	if len(os.Args) != 1 {
		args := os.Args[1:]
		optsRegex := regexp.MustCompile(`^\d+$`)
		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "-h" {
				fmt.Print("-h   Print this help message\n")
				fmt.Printf("-t   <INT>  Time to wait between screen draws (in ms), Default: %dms\n", wait_time)
				fmt.Printf("-H   <INT>  Graph height, Default: %d\n", graphHeight)
				fmt.Printf("-W   <INT>  Graph width, Default: %d\n", graphWidth)
				fmt.Printf("-P   <INT>  Graph precision, Default: %d\n", graphPrecision)
				fmt.Printf("-D   <INT>  Graph data points, Default: %d\n", dataPoints)
				fmt.Printf("--icmp      Preform ICMP checks, Default: %t\n", icmpCheck)
				fmt.Printf("-ip1 <ADDR> IP Address 1, Default: %s\n", addr1)
				fmt.Printf("-ip2 <ADDR> IP Address 2, Default: %s\n", addr2)
				fmt.Printf("-ip3 <ADDR> IP Address 3, Default: %s\n", addr3)

				return
			}

			if arg == "--icmp" {
				icmpCheck = true
			}

			if arg == "-t" && i+1 < len(args) {
				if optsRegex.MatchString(args[i+1]) {
					wait_time, err = strconv.Atoi(args[i+1])
					er(err)
				}
			}
			if arg == "-H" && i+1 < len(args) {
				if optsRegex.MatchString(args[i+1]) {
					graphHeight, err = strconv.Atoi(args[i+1])
					er(err)
				}
			}
			if arg == "-W" && i+1 < len(args) {
				if optsRegex.MatchString(args[i+1]) {
					graphWidth, err = strconv.Atoi(args[i+1])
					er(err)
				}
			}
			if arg == "-P" && i+1 < len(args) {
				if optsRegex.MatchString(args[i+1]) {
					tmpValue, err := strconv.ParseUint(args[i+1], 10, 64)
					er(err)
					graphPrecision = uint(tmpValue)
				}
			}
			if arg == "-D" && i+1 < len(args) {
				if optsRegex.MatchString(args[i+1]) {
					dataPoints, err = strconv.Atoi(args[i+1])
					er(err)
				}
			}
			if arg == "-ip1" && i+1 < len(args) {
				addr1 = args[i+1]
			}
			if arg == "-ip2" && i+1 < len(args) {
				addr2 = args[i+1]
			}
			if arg == "-ip3" && i+1 < len(args) {
				addr3 = args[i+1]
			}
		}
	}

	var loadavgs [5]float64
	var loadavgData [][]float64
	var loadavg1min []float64
	var loadavg5min []float64
	var loadavg15min []float64
	var loadavgPlot string

	var procSched []float64
	var procData [][]float64
	var procPlot string

	var memValues [6]float64
	var memData [][]float64
	var memFree []float64
	var memBuffer []float64
	var memCached []float64
	var memActive []float64
	var swapFree []float64
	var swapCache []float64
	var memPlot string

	var addr1Data []float64
	var addr2Data []float64
	var addr3Data []float64
	var addrData [][]float64
	var addrPlot string

	loadavgs = getLoadavg()
	loadavg1min = append(loadavg1min, loadavgs[0])
	loadavg5min = append(loadavg5min, loadavgs[1])
	loadavg15min = append(loadavg15min, loadavgs[2])
	procSched = append(procSched, loadavgs[4])

	loadavgData = append(loadavgData, loadavg1min)
	loadavgData = append(loadavgData, loadavg5min)
	loadavgData = append(loadavgData, loadavg15min)
	procData = append(procData, procSched)

	memValues = getMemory()
	memFree = append(memFree, memValues[0])
	memBuffer = append(memBuffer, memValues[1])
	memCached = append(memCached, memValues[2])
	memActive = append(memActive, memValues[5])
	swapFree = append(swapFree, memValues[4])
	swapCache = append(swapCache, memValues[3])
	//memData = append(memData, memFree)
	memData = append(memData, memBuffer)
	memData = append(memData, memCached)
	memData = append(memData, swapCache)
	memData = append(memData, swapFree)
	memData = append(memData, memActive)

	if icmpCheck {
		addr1Data = append(addr1Data, getICMP(addr1))
		addr2Data = append(addr2Data, getICMP(addr2))
		addr3Data = append(addr3Data, getICMP(addr3))
		addrData = append(addrData, addr1Data)
		addrData = append(addrData, addr2Data)
		addrData = append(addrData, addr3Data)
	}

	for {
		loadavgs = getLoadavg()
		loadavg1min = append(loadavg1min, loadavgs[0])
		loadavg5min = append(loadavg5min, loadavgs[1])
		loadavg15min = append(loadavg15min, loadavgs[2])
		procSched = append(procSched, loadavgs[4])

		memValues = getMemory()
		memFree = append(memFree, float64(memValues[0]))
		memBuffer = append(memBuffer, float64(memValues[1]))
		memCached = append(memCached, float64(memValues[2]))
		memActive = append(memActive, float64(memValues[5]))
		swapFree = append(swapFree, float64(memValues[4]))
		swapCache = append(swapCache, float64(memValues[3]))

		loadavgData[0] = append(loadavgData[0], loadavg1min...)
		loadavgData[1] = append(loadavgData[1], loadavg5min...)
		loadavgData[2] = append(loadavgData[2], loadavg15min...)
		procData[0] = append(procData[0], procSched...)
		//	memData[0] = append(memData[0], memFree...)
		memData[0] = append(memData[0], memBuffer...)
		memData[1] = append(memData[0], memCached...)
		memData[2] = append(memData[0], swapCache...)
		memData[3] = append(memData[0], swapFree...)
		memData[4] = append(memData[0], memActive...)

		if icmpCheck {
			addr1Data = append(addr1Data, getICMP(addr1))
			addr2Data = append(addr2Data, getICMP(addr2))
			addr3Data = append(addr3Data, getICMP(addr3))
			addrData[0] = addr1Data
			addrData[1] = addr2Data
			addrData[2] = addr3Data
		}

		fmt.Print("\033[H\033[2J")

		memLegend := fmt.Sprintf("Memory Usage (%s)", units)

		loadavgPlot = plotGraph(loadavgData, graphPrecision, graphWidth, graphHeight, "Load Averages", "1 min", "5 min", "15 min")
		procPlot = plotGraph(procData, graphPrecision, graphWidth, graphHeight, "Process/Threads", "Scheduling")
		//memPlot = plotGraph(memData, graphPrecision, graphWidth, graphHeight, memLegend, "Free", "Buffered", "Cached", "Swap Cached", "Swap Free", "Active")
		memPlot = plotGraph(memData, graphPrecision, graphWidth, graphHeight, memLegend, "Buffered", "Cached", "Swap Cached", "Swap Free", "Active")
		addrPlot = plotGraph(addrData, graphPrecision, graphWidth, graphHeight, "ICMP Response Time (ms)", addr1, addr2, addr3)

		fmt.Printf("\t\t\t1 min: %.02f\t5 min:%.02f\t15 min:%.02f\n", loadavg1min[len(loadavg1min)-1], loadavg5min[len(loadavg5min)-1], loadavg15min[len(loadavg15min)-1])
		fmt.Println(loadavgPlot)
		fmt.Printf("\t\t\tExecution: %.0f\tScheduled: %.0f\n", loadavgs[3], procSched[len(procSched)-1])
		fmt.Println(procPlot)
		fmt.Printf("\t\t\tFree: %.2f Buff: %.2f Cache: %.2f Swap Cache: %.2f Swap Free: %.2f Active: %.2f\n", memFree[len(memFree)-1], memBuffer[len(memBuffer)-1], memCached[len(memCached)-1], swapCache[len(swapCache)-1], swapFree[len(swapFree)-1], memActive[len(memActive)-1])
		fmt.Println(memPlot)
		if icmpCheck {
			fmt.Printf("\t\t\t%s: %.2f\t%s: %.2f\t%s: %.2f\n", addr1, addr1Data[len(addr1Data)-1], addr2, addr1Data[len(addr2Data)-1], addr3, addr1Data[len(addr3Data)-1])
			fmt.Println(addrPlot)
		}

		time.Sleep(time.Duration(wait_time) * time.Millisecond)
	}
}

func checksum(msg []byte) [2]byte {
	var tmp uint32

	if len(msg)%2 != 0 {
		msg = append(msg, 0)
	}

	for i := 0; i < len(msg); i++ {
		if i%2 != 0 {
			tmp += (uint32(msg[i-1]))<<8 + uint32(msg[i])
			tmp = (tmp & 0xFFFF) + (tmp >> 16)
		}
	}

	var cksum [2]byte
	cksum[0] = byte(tmp >> 8)
	cksum[1] = byte(tmp << 8 >> 8)
	cksum[0] = cksum[0] ^ 0xFF
	cksum[1] = cksum[1] ^ 0xFF

	return cksum
}

func getICMP(addr string) float64 {
	var icmpPacket []byte
	var startTime time.Time
	var stopTime time.Time
	var result float64

	icmpPacket = append(icmpPacket, 8, 0, 0, 0, 0, 0, 0, 0, 0x13, 0x37, 0x13, 0x37, 0x13, 0x37)
	cksum := checksum(icmpPacket)
	icmpPacket[2] = cksum[0]
	icmpPacket[3] = cksum[1]

	conn, err := net.Dial("ip4:1", addr)
	er(err)
	defer conn.Close()
	conn.SetDeadline(time.Now().Add(time.Duration(wait_time) * time.Millisecond))

	returnBuffer := make([]byte, 4500)

	startTime = time.Now()
	_, err = conn.Write(icmpPacket)
	_, err = conn.Read(returnBuffer)
	stopTime = time.Now()

	result = float64(time.Time.Sub(stopTime, startTime).Nanoseconds()) / 1000000
	return result
}

func plotGraph(data [][]float64, precision uint, width int, height int, legend string, names ...string) string {
	var plot string

	for i := 0; i < len(data); i++ {
		if len(data[i]) > dataPoints {
			data[i] = data[i][len(data[i])-dataPoints:]
		}
	}

	if len(data) >= 8 {
		plot = asciigraph.PlotMany(data,
			asciigraph.Precision(precision),
			asciigraph.SeriesColors(asciigraph.Green, asciigraph.Blue, asciigraph.Red, asciigraph.Yellow, asciigraph.Purple, asciigraph.White, asciigraph.Cyan, asciigraph.Gray),
			asciigraph.SeriesLegends(names...),
			asciigraph.Caption(legend),
			asciigraph.Width(width),
			asciigraph.Height(height))
	}
	if len(data) == 7 {
		plot = asciigraph.PlotMany(data,
			asciigraph.Precision(precision),
			asciigraph.SeriesColors(asciigraph.Green, asciigraph.Blue, asciigraph.Red, asciigraph.Yellow, asciigraph.Purple, asciigraph.White, asciigraph.Cyan),
			asciigraph.SeriesLegends(names...),
			asciigraph.Caption(legend),
			asciigraph.Width(width),
			asciigraph.Height(height))
	}
	if len(data) == 6 {
		plot = asciigraph.PlotMany(data,
			asciigraph.Precision(precision),
			asciigraph.SeriesColors(asciigraph.Green, asciigraph.Blue, asciigraph.Red, asciigraph.Yellow, asciigraph.Purple, asciigraph.White),
			asciigraph.SeriesLegends(names...),
			asciigraph.Caption(legend),
			asciigraph.Width(width),
			asciigraph.Height(height))
	}
	if len(data) == 5 {
		plot = asciigraph.PlotMany(data,
			asciigraph.Precision(precision),
			asciigraph.SeriesColors(asciigraph.Green, asciigraph.Blue, asciigraph.Red, asciigraph.Yellow, asciigraph.Purple),
			asciigraph.SeriesLegends(names...),
			asciigraph.Caption(legend),
			asciigraph.Width(width),
			asciigraph.Height(height))
	}
	if len(data) == 4 {
		plot = asciigraph.PlotMany(data,
			asciigraph.Precision(precision),
			asciigraph.SeriesColors(asciigraph.Green, asciigraph.Blue, asciigraph.Red, asciigraph.Yellow),
			asciigraph.SeriesLegends(names...),
			asciigraph.Caption(legend),
			asciigraph.Width(width),
			asciigraph.Height(height))
	}
	if len(data) == 3 {
		plot = asciigraph.PlotMany(data,
			asciigraph.Precision(precision),
			asciigraph.SeriesColors(asciigraph.Green, asciigraph.Blue, asciigraph.Red),
			asciigraph.SeriesLegends(names...),
			asciigraph.Caption(legend),
			asciigraph.Width(width),
			asciigraph.Height(height))
	}
	if len(data) == 2 {
		plot = asciigraph.PlotMany(data,
			asciigraph.Precision(precision),
			asciigraph.SeriesColors(asciigraph.Green, asciigraph.Blue),
			asciigraph.SeriesLegends(names...),
			asciigraph.Caption(legend),
			asciigraph.Width(width),
			asciigraph.Height(height))
	}
	if len(data) == 1 {
		plot = asciigraph.PlotMany(data,
			asciigraph.Precision(precision),
			asciigraph.SeriesColors(asciigraph.Green),
			asciigraph.SeriesLegends(names...),
			asciigraph.Caption(legend),
			asciigraph.Width(width),
			asciigraph.Height(height))
	}

	return plot
}

func getMemory() [6]float64 {
	var result [6]float64
	units = "KB"

	_, err := os.Stat(memFile)
	er(err)

	fh, err := os.Open(memFile)
	er(err)

	defer fh.Close()

	scanner := bufio.NewScanner(fh)
	for scanner.Scan() {
		line := scanner.Text()
		regex := regexp.MustCompile(`^(\S+)\s+(\d+)\s+kB$`)
		if regex.MatchString(line) {
			regexMatch := regex.FindStringSubmatch(line)

			tmpName := regexMatch[1]
			tmpValue, err := strconv.ParseFloat(regexMatch[2], 64)
			er(err)

			switch tmpName {
			case "MemFree:":
				result[0] = tmpValue
			case "Buffers:":
				result[1] = tmpValue
			case "Cached:":
				result[2] = tmpValue
			case "SwapCached:":
				result[3] = tmpValue
			case "SwapFree:":
				result[4] = tmpValue
			case "Active:":
				result[5] = tmpValue
			}
		}
	}

	var max float64
	for i := 0; i < len(result); i++ {
		if result[i] > max {
			max = result[i]
		}
	}

	if max/1024 > 0 {
		units = "MB"
		for i := 0; i < len(result); i++ {
			result[i] /= 1024
		}
	}

	if max/1024/1024 > 0 {
		units = "GB"
		for i := 0; i < len(result); i++ {
			result[i] /= 1024
		}
	}

	return result
}

func getLoadavg() [5]float64 {
	var result [5]float64

	_, err := os.Stat(cpuFile)
	er(err)

	fhString, err := os.ReadFile(cpuFile)
	er(err)

	regex := regexp.MustCompile(`^(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+)\/(\d+)`)
	if regex.MatchString(string(fhString)) {
		regexMatch := regex.FindStringSubmatch(string(fhString))

		result[0], err = strconv.ParseFloat(regexMatch[1], 64)
		er(err)
		result[1], err = strconv.ParseFloat(regexMatch[2], 64)
		er(err)
		result[2], err = strconv.ParseFloat(regexMatch[3], 64)
		er(err)
		result[3], err = strconv.ParseFloat(regexMatch[4], 64)
		er(err)
		result[4], err = strconv.ParseFloat(regexMatch[5], 64)
		er(err)

	}

	return result
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
