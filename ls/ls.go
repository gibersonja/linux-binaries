package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
)

var files []string

func main() {
	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {

			} else {
				fileInfo, err := os.Stat(arg)
				er(err)

				fileInfo.Name()
				files = append(files, fileInfo.Name())
			}
		}
	}

	for i := 0; i < len(files); i++ {
		fileInfo, err := os.Stat(files[i])
		er(err)

		if fileInfo.IsDir() {
			dirContents, err := os.ReadDir(files[i])
			er(err)

			for j := 0; j < len(dirContents); j++ {
				fileType := dirContents[j].Type()
				fileName := dirContents[j].Name()

				fmt.Printf("%s\n", fileName)
				fmt.Printf("%032b\n", fileType)
				fmt.Printf("%v\n", fileType)
			}
		}
	}

}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
