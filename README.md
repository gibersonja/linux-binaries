# linux binaries



## To Do
- Binaries to create to be compiled for windows, mostly rewritten coreutils
  - [Network](#network)
    - [x] [netmon](#netmon)
    - [x] [ping](#ping)
    - [ ] [dhcpserver](#dhcpserver)
    - [x] [icuping](#icuping)
    - [x] [netscan](#netscan)
    - [x] [ipcalc [netcalc]](#netcalc)
    - [ ] traceroute
    - [ ] nc
  - [File Operations](#file-operations)
    - [ ] ls
    - [ ] grep
    - [x] [cat](#cat)
    - [ ] nl
    - [x] [wc](#wc)
    - [ ] awk
    - [ ] lsof
    - [ ] tail
    - [ ] head - Almost finished
    - [ ] watch
    - [ ] wall
    - [x] [case](#case)
    - [x] [basename](#basename)
    - [x] [dirname](#dirname)
    - [ ] comm
    - [ ] diff
    - [ ] sdiff
    - [ ] cmp
    - [ ] patch
    - [ ] od
    - [x] [seq](#seq)
    - [ ] tr
    - [x] [expand](#expland)
    - [ ] unexpand
    - [ ] date
    - [ ] tee
    - [ ] cksum
    - [ ] cut
    - [x] [pwd](#pwd)
    - [x] [time [proctime]](#proctime)
    - [x] [hexdump](#hexdump)
  - [Monnitoring](#monitoring)
    - [X] [sysmon](#sysmon)
    - [ ] [pstree](#pstree)
  

  ## Network
  ### netmon
  - ![netmon screenshot](.screenshots/netmon-screenshot.png "netmon screenshot")
  - ```
      ./netmon -h
      -h | --help      Print this help message
      -t | --timeout   Specify ICMP timout in ms
      -w | --wait      Specify time to wait between ICMP packet per host
      -g | --graph     Disable graph display
      -H | --height    Graph height, Default: 25d
      -W | --width     Graph width, Default: 100
      -P | --precision Graph precision, Default: 2
      -c | --color     Turn off color, suggested for non ANSI terminal
  ### ping
  - ```
      ./ping --help
      -h | --help  Print this help message
      -T | --time  Milliseconds to wait between requests
  ### dhcpserver
  - Simple DHCP server
  ### icuping
  - ```
      ./icuping --help
      --type=<#>     ICMP Type (uint8)
      --code=<#>     Code Integer (uint8)
      --data=<#>     Data (uint64)
      --cksum=<#>    Message Checksum (uint16), if ommited checksum will be calculated automatically
      --ptr=<#>      Pointer (Type 12) (uint8)
      --gw=<A.B.C.D> Gateway Internet Address (Type 5) (uint32)
      --id=<#>       Identifier (Type 8,0,13,14,15,16) (uint16)
      --seq=<#>      Sequence Number (Type 8,0,13,14,15,16) (uint16)
      --org_t=<#>    Originate Timestamp (Type 13,14) (uint32)
      --rcv_t=<#>    Receive Timestamp (Type 13,14) (uint32)
      --trn_t=<#>    Transmit Timestamp (Type 13,14) (uint32)
      -q             Turn off verbose output
      W.X.Y.Z        Destination IP address
      --rfc          Print the packet structure per RFC
      --help         Print this help message


      Reference:     RFC792
  ### netscan
  - scan via TCP and ICMP, basically a copy of goscan
  - ```
      ./netscan -h
      -h | --help            Print this help message
      -v | --verbose         Verbose output
      (-t | --timeout) <int>  Connection timeout in ms, Default: 50000000 ms
      -a | --alive           Only show alive hosts
      (-o | --output) <file>  Write output to file
      --icmp                  Specify ICMP scan, Default: false
      --tcp                   Specify TCP scan, Default: false
      --udp                   Specify UDP scan, Default: false
      --ports=A-C,E,G         Specify ports to scan, to be used with --tcp or --udp
      A.B.C.D                 Single IP address to scan
      A.B.C.D[/XY]            IP address and mask bit to specify scan range
      A.B.C.D-E.F.G.H         IP address range to scan
  ### netcalc
  - My own version of ipcalc
  - ```
      ./netcalc -h
      -h | --help     Print this help message.
      -q | --quiet    Do not print normal output
      -v | --verbose  Show verbose output
      A.B.C.D/XY      CIDR Address to calculate the IP range.
  - ```
      ./netcalc 1.2.3.4/30
      1.2.3.4
      1.2.3.5
      1.2.3.6
      1.2.3.7
  - ```
      ./netcalc 1.2.3.4/30 -v
      NETWORK ADDR:   00000001.00000010.00000011.00000100     1.2.3.4
      BROADCAST ADDR: 00000001.00000010.00000011.00000111     1.2.3.7
      NETWORK MASK:   11111111.11111111.11111111.11111100     255.255.255.252
      HOST MASK:      00000000.00000000.00000000.00000011     0.0.0.3

      1.2.3.4
      1.2.3.5
      1.2.3.6
      1.2.3.7
  - ```
      ./netcalc 1.2.3.4/30 -vq
      NETWORK ADDR:   00000001.00000010.00000011.00000100     1.2.3.4
      BROADCAST ADDR: 00000001.00000010.00000011.00000111     1.2.3.7
      NETWORK MASK:   11111111.11111111.11111111.11111100     255.255.255.252
      HOST MASK:      00000000.00000000.00000000.00000011     0.0.0.3
  
  ## File Operations
  ### cat
  - ```
    ./cat --help
    -A, --show-all
          equivalent to -vET

    -b, --number-nonblank
          number nonempty output lines, overrides -n

    -e     equivalent to -vE

    -E, --show-ends
          display $ at end of each line

    -n, --number
          number all output lines

    -s, --squeeze-blank
          squeeze repeated empty output lines

    -t     equivalent to -vT

    -T, --show-tabs
          display TAB characters as ^I

    -u     (ignored)

    -v, --show-nonprinting
          use ^ and M- notation, except for LFD and TAB

    --help display this help and exit

    --version
          (ignored)
       
  ### wc
  - Currently discrepancy for line count (`-l && --lines`).   Likely due to the fact that my code account for `CR` EOL character as well as `CRLF` and `LF`. Example:
    - ```
      ./wc --help
      -c, --bytes
            print the byte counts

      -m, --chars
            print the character counts

      -l, --lines
            print the newline counts

      -L, --max-line-length
            print the maximum display width

      -w, --words
            print the word counts

      --help display this help message
  ### case
  - Convert uppercase to lowercase and vice versa:
    - ```
        ./case --help
        --help  Print this help message
        -u      Convert to uppercase only
        -l      Convert to lowercase only
        Only accepts input from STDIN

  ### basename
  - ```
      ./basname --help
      -a, --multiple
            support multiple arguments and treat each as a NAME

      -s, --suffix=SUFFIX
            remove a trailing SUFFIX; implies -a

      -z, --zero
            end each output line with NUL, not newline

      --help display this help and exit

  ### dirname
  - ```
    ./dirname --help
    -z, --zero
          end each output line with NUL, not newline

    --help display this help and exit

  ### seq
  - ```
      ./seq --help
      -f, --format=FORMAT
            use printf style floating-point FORMAT

      -s, --separator=STRING
            use STRING to separate numbers (default: \n)

      -w, --equal-width
            equalize width by padding with leading zeros

      --help display this help and exit
      
  ### expand
  - ``` bash
        ./expand --help
        -i, --initial
              do not convert tabs after non blank lines

        -t, --tabs=N
              have tabs N characters apart, not 8

        -t, --tabs=LIST
              use comma separated list of tab positions The las specified position can be prefixed with '/' to specify a tab size to use after the last explicitly specifed tab stop.  Also a prefix of '+' can be used to align remaining tab stops relative to the last specified tab stop instead of the first column
  
  ### proctime
  - Similar to the time command, but only shows process time.  Does not measure user or system time.
  - ```
      ./proctime ls -lth -a
      total 2.3M
      drwxr-xr-x  2 jgiberson jgiberson 4.0K Jun 21 09:20 .
      -rwxr-xr-x  1 jgiberson jgiberson 2.3M Jun 21 09:20 proctime
      -rw-r--r--  1 jgiberson jgiberson  809 Jun 21 09:20 proctime.go
      drwxr-xr-x 17 jgiberson jgiberson 4.0K Jun 21 09:03 ..
      -rw-r--r--  1 jgiberson jgiberson   27 Jun 21 09:00 go.mod
      982.83µs
  ### pwd
  - Self explanitory
 ### hexdump
- ```
      ./hexdump --help
      --help       Print this help message
      --offset <#> Byte offset to start at (decimal not hexadecimal)

 - ```
      ./hexdump ../test_files/eol
      0x00: 616C 7366 6B20 2020 6173 6C64 6664 6173   l   s   f   k   SPC SPC SPC a   s   l   d   f   d   a   s   l
      0x10: 6C66 6B20 6173 6466 0A61 6C73 6466 0A61   f   k   SPC a   s   d   f   LF  a   l   s   d   f   LF  a   s
      0x20: 7364 6173 2020 2061 7366 6120 2020 2020   d   a   s   SPC SPC SPC a   s   f   a   SPC SPC SPC SPC SPC SPC
      0x30: 2020 2061 7364 0909 6173 6466 2020 2020   SPC SPC a   s   d   TAB TAB a   s   d   f   SPC SPC SPC SPC SPC
      0x40: 2020 2020 2061 7364 6620 2020 2020 2020   SPC SPC SPC SPC a   s   d   f   SPC SPC SPC SPC SPC SPC SPC SPC
      0x50: 2020 2020 2020 2020 2020 2020 2020 2020   SPC SPC SPC SPC SPC SPC SPC SPC SPC SPC SPC SPC SPC SPC SPC SPC
      0x60: 2064 6173 640A 0A36 3435 3534 3334 3609   d   a   s   d   LF  LF  6   4   5   5   4   3   4   6   TAB 5
      0x70: 3534 360D 0A61 736B 6466 0A0A 0A61 7366   4   6   CR  LF  a   s   k   d   f   LF  LF  LF  a   s   f   LF
      0x80: 0A61 6C73 0909 6466 6664 6161 730A 6461   a   l   s   TAB TAB d   f   f   d   a   a   s   LF  d   a   s
      0x90: 7364 6173 0A0D 7364 616F 6173 6F70 6166   d   a   s   LF  CR  s   d   a   o   a   s   o   p   a   f   3
      0xA0: 3332 340D 0D0A                            3   2   4   CR  CR  LF
## Monitoring
  ### sysmon
  ```
sysmon -h
-h   Print this help message
-t   <INT>  Time to wait between screen draws (in ms), Default: 300ms
-H   <INT>  Graph height, Default: 10
-W   <INT>  Graph width, Default: 100
-P   <INT>  Graph precision, Default: 2
-D   <INT>  Graph data points, Default: 50
--icmp      Preform ICMP checks, Default: false
-ip1 <ADDR> IP Address 1, Default: 8.8.8.8
-ip2 <ADDR> IP Address 2, Default: 208.67.222.222
-ip3 <ADDR> IP Address 3, Default: 1.1.1.1
```
  - ![sysmon screenshot](.screenshots/sysmon.png "sysmon screenshot")
  ### pstree
