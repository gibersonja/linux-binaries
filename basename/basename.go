package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
)

var multipleFiles bool
var suffixCheck bool
var zero bool
var fileNames []string
var suffix string

func main() {
	regex := regexp.MustCompile(`^--suffix=(\S+)$`)
	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("-a, --multiple\n")
				fmt.Print("       support multiple arguments and treat each as a NAME\n\n")
				fmt.Print("-s, --suffix=SUFFIX\n")
				fmt.Print("       remove a trailing SUFFIX; implies -a\n\n")
				fmt.Print("-z, --zero\n")
				fmt.Print("       end each output line with NUL, not newline\n\n")
				fmt.Print("--help display this help and exit\n")

				return
			} else if arg == "-a" || arg == "--multiple" {
				multipleFiles = true
			} else if arg == "-s" || regex.MatchString(arg) {
				suffixCheck = true
				if arg == "-s" && i+1 < len(args) {
					suffix = args[i+1]
					i++
				} else {
					suffix = regex.FindStringSubmatch(arg)[1]
				}
			} else if arg == "-z" || arg == "--zero" {
				zero = true
			} else {
				fileNames = append(fileNames, arg)
			}
		}
	}

	if len(fileNames) == 0 {
		er(fmt.Errorf("missing operand"))
	}
	if multipleFiles {
		for i := 0; i < len(fileNames); i++ {
			fileName := fileNames[i]
			getBasename(fileName)
		}
	} else {
		getBasename(fileNames[0])
	}
}

func getBasename(fileName string) {
	regex := regexp.MustCompile(`.*[/]([^/]+)$`)
	if len(regex.FindStringSubmatch(fileName)) > 1 {
		fileName = regex.FindStringSubmatch(fileName)[1]
	}

	if suffixCheck {
		suffixString := fmt.Sprintf("(.+)%s", suffix)
		suffixRegex := regexp.MustCompile(suffixString)
		if len(suffixRegex.FindStringSubmatch(fileName)) > 1 {
			fileName = suffixRegex.FindStringSubmatch(fileName)[1]
		}
	}

	if !zero {
		fmt.Printf("%s\n", fileName)
	} else {
		fmt.Printf("%s%c", fileName, 0)
	}

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
