package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"runtime"
)

var files []string
var base int = 8 // 0 = string

func main() {
	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("--help  Print this help message\n")

				return
			} else if arg == "-x" {
				base = 16
			} else if arg == "-s" {
				base = 10
			} else {
				_, err := os.Stat(arg)
				er(err)

				files = append(files, arg)
			}
		}
	}

	for i := 0; i < len(files); i++ {
		file := files[i]
		fileInfo, err := os.Stat(file)
		er(err)

		fh, err := os.Open(file)
		er(err)
		defer fh.Close()

		for j := int64(0); j < fileInfo.Size(); j++ {
			readBuf := make([]byte, 1)
			_, err = fh.ReadAt(readBuf, j)
			if err != nil && err != io.EOF {
				er(err)
			}

			if j%16 == 0 {
				if j != 0 {
					fmt.Print("\n")
				}
				fmt.Printf("%07o ", j)
			}

			if base == 8 {
				fmt.Printf("%03o", readBuf[0])
			}
			if base == 16 {
				fmt.Printf("%02x", readBuf[0])
			}
			if base == 10 {
				fmt.Printf("%02d", readBuf[0])
			}

			if j%2 == 1 && j%16 != 0 {
				fmt.Print(" ")
			}
		}

		fmt.Printf("\n%07o\n", fileInfo.Size())

	}
}

func er(err error, cleanup ...string) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		if len(cleanup) > 0 {
			if cleanup[0] != "" {
				_, err := os.Stat(cleanup[0])
				er(err, "")
				err = os.Remove(cleanup[0])
				er(err, "")
			}
		}
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
