package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
	"strings"
)

var data []byte
var numberLines bool
var numberNonBlank bool
var lineCount uint64 = 1
var nonPrinting bool
var squeezeBlank bool
var showTabs bool
var showEnds bool

func main() {

	// Pipe is not reading LF for some reason???
	/*
		if isPipe() {
			rPipe, _ := io.Pipe()
			_, err := rPipe.Read(data)
			er(err)
		}
	*/
	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" {
				fmt.Print("-A, --show-all\n")
				fmt.Print("       equivalent to -vET\n\n")
				fmt.Print("-b, --number-nonblank\n")
				fmt.Print("       number nonempty output lines, overrides -n\n\n")
				fmt.Print("-e     equivalent to -vE\n\n")
				fmt.Print("-E, --show-ends\n")
				fmt.Print("       display $ at end of each line\n\n")
				fmt.Print("-n, --number\n")
				fmt.Print("       number all output lines\n\n")
				fmt.Print("-s, --squeeze-blank\n")
				fmt.Print("       squeeze repeated empty output lines\n\n")
				fmt.Print("-t     equivalent to -vT\n\n")
				fmt.Print("-T, --show-tabs\n")
				fmt.Print("       display TAB characters as ^I\n\n")
				fmt.Print("-u     (ignored)\n\n")
				fmt.Print("-v, --show-nonprinting\n")
				fmt.Print("       use ^ and M- notation, except for LFD and TAB\n\n")
				fmt.Print("--help display this help and exit\n\n")
				fmt.Print("--version\n")
				fmt.Print("       (ignored)\n")

				return
			} else if arg == "-b" || arg == "--number-nonblank" {
				numberNonBlank = true
			} else if arg == "-v" || arg == "--show-nonprinting" {
				nonPrinting = true
			} else if arg == "-E" || arg == "--show-ends" {
				showEnds = true
			} else if arg == "-n" || arg == "--number" {
				numberLines = true
			} else if arg == "-s" || arg == "--squeeze-blank" {
				squeezeBlank = true
			} else if arg == "-T" || arg == "--show-tabs" {
				showTabs = true
			} else if arg == "-vE" || arg == "-e" {
				nonPrinting = true
				showEnds = true

			} else if arg == "-vT" || arg == "-t" {
				showTabs = true
				nonPrinting = true
			} else if arg == "-vET" || arg == "--show-all" || arg == "-A" {
				nonPrinting = true
				showEnds = true
				showTabs = true
			} else if arg == "-u" || arg == "--version" {
				continue
			} else { // add support for filename globbing
				fhInfo, err := os.Stat(arg)
				er(err)

				fileName := arg
				fileSize := fhInfo.Size()

				fh, err := os.Open(fileName)
				er(err)
				defer fh.Close()

				fileBuffer := make([]byte, fileSize)
				_, err = bufio.NewReader(fh).Read(fileBuffer)
				if err != nil && err != io.EOF {
					er(err)
				}

				data = fileBuffer
			}
		}
	}

	var cr byte = 13
	var lf byte = 10
	var tab byte = 9
	var stringData []string
	var start int
	var stop int

	for i := 0; i < len(data); i++ {
		if i-1 >= 0 && ((data[i-1] == cr && data[i] == lf) || (data[i-1] == cr && data[i] != lf) || (data[i-1] != cr && data[i] == lf)) {
			stop = i + 1
			stringData = append(stringData, string(data[start:stop]))
			start = stop
		}
	}

	var line int = 1
	regex := regexp.MustCompile(`^\s*$`)
	for i := 0; i < len(stringData); i++ {
		var output string
		if numberLines || (numberNonBlank && !regex.MatchString(stringData[i])) {

			output = fmt.Sprintf("%6d %s", line, stringData[i])
			line++

		} else if numberNonBlank && regex.MatchString(stringData[i]) {

			output = fmt.Sprintf("       %s", stringData[i])
		} else if i-1 > -1 && squeezeBlank && stringData[i-1] == stringData[i] {
			continue
		} else {
			output = stringData[i]
		}

		if nonPrinting {
			charMap := make(map[int]string)

			charMap[0] = "^@"
			charMap[1] = "^A"
			charMap[2] = "^B"
			charMap[3] = "^C"
			charMap[4] = "^D"
			charMap[5] = "^E"
			charMap[6] = "^F"
			charMap[7] = "^G"
			charMap[8] = "^H"
			charMap[9] = "^I"
			charMap[10] = "^J"
			charMap[11] = "^K"
			charMap[12] = "^L"
			charMap[13] = "^M"
			charMap[14] = "^N"
			charMap[15] = "^O"
			charMap[16] = "^P"
			charMap[17] = "^Q"
			charMap[18] = "^R"
			charMap[19] = "^S"
			charMap[20] = "^T"
			charMap[21] = "^U"
			charMap[22] = "^V"
			charMap[23] = "^W"
			charMap[24] = "^X"
			charMap[25] = "^Y"
			charMap[26] = "^Z"
			charMap[27] = "^["
			charMap[28] = "^\\"
			charMap[29] = "^]"
			charMap[30] = "^^"
			charMap[31] = "^_"
			charMap[127] = "^?"

			//dont want to replace eol characters, just add the ^symbol before the eol
			for n := 0; n < len(output); n++ {
				character := output[n]

				if (int(character) > 0 && int(character) < 32 || int(character) == 127) && int(character) != 10 && int(character) != 9 {
					output = strings.Replace(output, string(character), charMap[int(character)], 1)
				}
			}
		}

		if showTabs {
			for n := 0; n < len(output); n++ {
				character := output[n]
				if byte(character) == tab {
					output = strings.Replace(output, string(character), "^I", 1)
				}
			}
		}

		if showEnds {
			for n := 0; n < len(output); n++ {
				character := output[n]
				if byte(character) == cr {
					output = strings.Replace(output, string(character), "^M", 1)
				}
				if byte(character) == lf {
					output = strings.Replace(output, string(character), fmt.Sprint("$\n"), 1)
					n++
				}
			}
		}

		fmt.Print(output)
	}

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
