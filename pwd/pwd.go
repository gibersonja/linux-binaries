package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
)

func main() {
	execInfo, err := os.Executable()
	er(err)

	fmt.Println(execInfo)
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
